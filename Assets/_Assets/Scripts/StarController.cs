﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.EventSystems;

public class StarController : MonoBehaviour
{
    public Animator starAnim;
    public ScanController scanController;
    private BoxCollider boxCollider;
    private bool isPaused = false;
    public GameObject TapMeIndicators;
    public bool isSending = false;

    private void Awake()
    {
        boxCollider = GetComponent<BoxCollider>();
    }

    private void OnEnable()
    {
        boxCollider.enabled = isPaused;
        isSending = false;
    }

    private void OnDisable()
    {   
        if(!isPaused)
        {
            isSending = false;
            isPaused = false;
            
        }
        StopAllCoroutines();
    }

    public void IdleState()
    {
        boxCollider.enabled = true;
        isPaused = true;
    }

    private void OnMouseDown()
    {
        if(EventSystem.current.IsPointerOverGameObject(-1))
        {
            return;
        }
        if(!isSending)
        {

            starAnim.SetTrigger("TapTrigger");
        }
    }

    public void Tapped()
    {
        isPaused = false;
    }

    public void TapSendScan()
    {
        isPaused = false;
        TapMeIndicators.SetActive(false);
        StartCoroutine(SendScan());
    }


    private IEnumerator SendScan()
    {
        isPaused = false;
        isSending = true;
        yield return new WaitForSecondsRealtime(3f);
        scanController.OnBoxTap(delegate
        {
            isPaused = isSending = false;
            TapMeIndicators.SetActive(false);
            UIManager.Instance.Marker.SetActive(false);
        });
    }
}
