﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using TMPro;
using UnityEngine.PlayerLoop;
using UnityEngine.Video;

public class PoseCountDown : MonoBehaviour
{
    public AnimatedUIEntry captureButton;
    public AnimatedUIEntry countDownScreen;
    public TextMeshProUGUI txtTimer;
    public GameObject santaObjectToShow;
    public GameObject santaObjectToShow2;
    public GameObject santaObjectToShow3;
    public GameObject santaObjectToShow4;
    public GameObject santaObjectToShow5;
    public Sprite santaSilhoutte1;
    public Sprite santaSilhoutte2;
    public Sprite santaSilhoutte3;
    public Sprite santaSilhoutte4;
    public Sprite santaSilhoutte5;
    float normalizedTime = 4f;
    public float timer = 3f;
    public bool timerEnabled = false;
    public Texture transparent;
    private void Start()
    {
        //ResetTimer(4f);
        //StartCoroutine(Countdown(3));
        //Time.timeScale = 1;
        //timer = 3f;
    }
    private void OnEnable()
    {
        timerEnabled = true;
        UIManager.Instance.EnableMusicPLayer();
        
    }

    private void OnDisable()
    {
        timer = 3f;
        timerEnabled = false;
    }

    private void Update()
    {
        if (UIManager.Instance.santaMarkerID == 1)
        {
            UIManager.Instance.santaPoseSilhouette.sprite = santaSilhoutte1;
            
        }
        else if (UIManager.Instance.santaMarkerID == 2)
        {
            UIManager.Instance.santaPoseSilhouette.sprite = santaSilhoutte2;
            
        }
        else if (UIManager.Instance.santaMarkerID == 3)
        {
            UIManager.Instance.santaPoseSilhouette.sprite = santaSilhoutte3;
            
        }
        else if (UIManager.Instance.santaMarkerID == 4)
        {
            UIManager.Instance.santaPoseSilhouette.sprite = santaSilhoutte4;
        }
        else if (UIManager.Instance.santaMarkerID == 5)
        {
            UIManager.Instance.santaPoseSilhouette.sprite = santaSilhoutte5;
            
        }

        if (timerEnabled)
        {
            timer -= Time.deltaTime;
            txtTimer.text = timer.ToString("0");
            if (timer <= 0)
            {
                timer = 3f;
#if UNITY_IOS
                StartCoroutine(DoStuff());
#elif UNITY_ANDROID && !UNITY_EDITOR
                timerEnabled = false;
#else
                StartCoroutine(DoStuff());
#endif
            }
#if UNITY_ANDROID && !UNITY_EDITOR
            if (timer <= 1)
            {

                StartCoroutine(DoStuff());
            }
#endif
        }

    }



    public void ResetTimer(float _timer)
    {
        normalizedTime = _timer;
    }

    IEnumerator DoStuff()
    {
        FirebaseChecker.Instance.LogFirebaseEvent("Santa_Strike_Pose");
#if UNITY_IOS
        countDownScreen.CallExitAnimation(-1);
        captureButton.transform.parent.gameObject.SetActive(true);
        captureButton.CallEntryAnimation(1);
        yield return new WaitForEndOfFrame();
#elif UNITY_EDITOR
        countDownScreen.CallExitAnimation(-1);
        captureButton.transform.parent.gameObject.SetActive(true);
        captureButton.CallEntryAnimation(1);
        yield return new WaitForEndOfFrame();
#endif

        if (UIManager.Instance.santaMarkerID == 1)
        {
            FirebaseChecker.Instance.LogFirebaseEvent("Santa_ARView_scanned_marker1");
            UIManager.Instance.santaPoseSilhouette.sprite = santaSilhoutte1;
            UIManager.Instance.santaPoseContainer.SetActive(true);
            santaObjectToShow.SetActive(true);
#if UNITY_ANDROID
            UIManager.Instance.santaPoseVid1.targetTexture.DiscardContents();
            UIManager.Instance.santaPoseVid1.targetTexture.Release();
            UIManager.Instance.santaPoseVid1.prepareCompleted += PrepareCompleteVid1;

            Graphics.Blit(transparent, UIManager.Instance.santaPoseVid1.targetTexture);
#elif UNITY_IOS
            UIManager.Instance.santaPoseVid5.prepareCompleted += PrepareCompleteVid5;
#endif

        }
        else if (UIManager.Instance.santaMarkerID == 2)
        {
            FirebaseChecker.Instance.LogFirebaseEvent("Santa_ARView_scanned_marker2");
            UIManager.Instance.santaPoseSilhouette.sprite = santaSilhoutte2;
            UIManager.Instance.santaPoseContainer.SetActive(true);
            santaObjectToShow2.SetActive(true);
#if UNITY_ANDROID
            UIManager.Instance.santaPoseVid2.targetTexture.DiscardContents();
            UIManager.Instance.santaPoseVid2.targetTexture.Release();
            UIManager.Instance.santaPoseVid2.prepareCompleted += PrepareCompleteVid2;

            Graphics.Blit(transparent, UIManager.Instance.santaPoseVid2.targetTexture);
#elif UNITY_IOS
            UIManager.Instance.santaPoseVid5.prepareCompleted += PrepareCompleteVid5;
#endif
        }
        else if (UIManager.Instance.santaMarkerID == 3)
        {
            FirebaseChecker.Instance.LogFirebaseEvent("Santa_ARView_scanned_marker3");
            UIManager.Instance.santaPoseSilhouette.sprite = santaSilhoutte3;
            UIManager.Instance.santaPoseContainer.SetActive(true);
            santaObjectToShow3.SetActive(true);
#if UNITY_ANDROID
            UIManager.Instance.santaPoseVid3.targetTexture.DiscardContents();
            UIManager.Instance.santaPoseVid3.targetTexture.Release();
            UIManager.Instance.santaPoseVid3.prepareCompleted += PrepareCompleteVid3;

            Graphics.Blit(transparent, UIManager.Instance.santaPoseVid3.targetTexture);
#elif UNITY_IOS
            UIManager.Instance.santaPoseVid5.prepareCompleted += PrepareCompleteVid5;
#endif
        }
        else if (UIManager.Instance.santaMarkerID == 4)
        {
            FirebaseChecker.Instance.LogFirebaseEvent("Santa_ARView_scanned_marker4");
            UIManager.Instance.santaPoseSilhouette.sprite = santaSilhoutte4;
            UIManager.Instance.santaPoseContainer.SetActive(true);
            santaObjectToShow4.SetActive(true);
#if UNITY_ANDROID
            UIManager.Instance.santaPoseVid4.targetTexture.DiscardContents();
            UIManager.Instance.santaPoseVid4.targetTexture.Release();
            UIManager.Instance.santaPoseVid4.prepareCompleted += PrepareCompleteVid4;

            Graphics.Blit(transparent, UIManager.Instance.santaPoseVid4.targetTexture);
#elif UNITY_IOS
            UIManager.Instance.santaPoseVid4.prepareCompleted += PrepareCompleteVid5;
#endif

        }
        else if (UIManager.Instance.santaMarkerID == 5)
        {
            FirebaseChecker.Instance.LogFirebaseEvent("Santa_ARView_scanned_marker5");
            UIManager.Instance.santaPoseSilhouette.sprite = santaSilhoutte5;
            UIManager.Instance.santaPoseContainer.SetActive(true);
            santaObjectToShow5.SetActive(true);

#if UNITY_ANDROID
            UIManager.Instance.santaPoseVid5.targetTexture.DiscardContents();
            UIManager.Instance.santaPoseVid5.targetTexture.Release();
            UIManager.Instance.santaPoseVid5.prepareCompleted += PrepareCompleteVid5;

            Graphics.Blit(transparent, UIManager.Instance.santaPoseVid5.targetTexture);
#elif UNITY_IOS
            UIManager.Instance.santaPoseVid5.prepareCompleted += PrepareCompleteVid5;
#endif
        }
        Time.timeScale = 1;
#if UNITY_ANDROID && !UNITY_EDITOR
        yield return new WaitForEndOfFrame();
        countDownScreen.CallExitAnimation(-1);
        captureButton.transform.parent.gameObject.SetActive(true);
        captureButton.CallEntryAnimation(-1);
#endif
    }


    private void PrepareCompleteVid1(VideoPlayer videoPlayer)
    {
        videoPlayer.Play();
    }

    private void PrepareCompleteVid2(VideoPlayer videoPlayer)
    {
        videoPlayer.Play();
    }

    private void PrepareCompleteVid3(VideoPlayer videoPlayer)
    {
        videoPlayer.Play();
    }

    private void PrepareCompleteVid4(VideoPlayer videoPlayer)
    {
        videoPlayer.Play();
    }

    private void PrepareCompleteVid5(VideoPlayer videoPlayer)
    {
        videoPlayer.Play();
    }

}
