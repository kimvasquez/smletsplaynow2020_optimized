﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class FirebaseChecker : MonoBehaviour {


    private static FirebaseChecker _instance;
    public static FirebaseChecker Instance
    {
        get
        {
            if (_instance == null)
            {
                _instance = FindObjectOfType<FirebaseChecker>();
                if (_instance == null)
                {
                    GameObject go = new GameObject();
                    go.name = typeof(FirebaseChecker).Name;
                    _instance = go.AddComponent<FirebaseChecker>();
                    DontDestroyOnLoad(go);
                }
            }
            return _instance;
        }
    }

    public bool isFirebaseAvailable = false;


    private void Awake()
    {
#if UNITY_ANDROID
        Firebase.FirebaseApp.CheckAndFixDependenciesAsync().ContinueWith(task => {
            var dependencyStatus = task.Result;
            if (dependencyStatus == Firebase.DependencyStatus.Available)
            {
                isFirebaseAvailable = true;
            }
            else
            {
                isFirebaseAvailable = false;
            }
        });

#elif UNITY_IOS
           isFirebaseAvailable = true;
#endif
    }

    public void LogFirebaseEvent(string eventName)
    {
        Debug.Log(eventName);
        if (isFirebaseAvailable)
        {
#if UNITY_ANDROID
            Firebase.Analytics.FirebaseAnalytics.LogEvent(eventName + "_Android_SM2020", "value", 1);
#elif UNITY_IOS
            Firebase.Analytics.FirebaseAnalytics.LogEvent(eventName + "_iOS_SM2020", "value", 1);
#endif
        }
    }

}
