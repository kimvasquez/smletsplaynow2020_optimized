﻿public class AppConstants
{
    public static readonly string BASE_URL = "https://lpn.boomtech.co/api";

    public static readonly string LOGIN_URL = "/login";
    public static readonly string REGISTER_URL = "/register";
    public static readonly string RESET_PASS_URL = "/password/email";
    public static readonly string CHANGE_PASS_URL = "/password/change";
    public static readonly string VERIFICATION_URL = "/send-email-verification";

    public static readonly string REWARDS_URL = "/rewards";
    public static readonly string SCANS_URL = "/scans";

    public static readonly string APP_VERSION_URL = "/version";

    public static readonly string UPLOAD_URL = "/upload/photo";
}
