﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Networking;
using System;
using System.Reflection;

public class RestfulAPI : MonoBehaviour
{
    private static RestfulAPI _instance;

    public static RestfulAPI Instance
    {
        get
        {
            if (_instance == null)
            {
                _instance = FindObjectOfType<RestfulAPI>();
                if (_instance == null)
                {
                    GameObject go = new GameObject();
                    go.name = typeof(RestfulAPI).Name;
                    _instance = go.AddComponent<RestfulAPI>();
                    DontDestroyOnLoad(go);
                }
            }
            return _instance;
        }
    }

    private static readonly int WEB_REQUEST_TIMEOUT = 10;

    /*****************************USER MANAGEMENT API CALL********************************/
    /// <summary>
    /// Call a web request with custom callback
    /// </summary>
    /// <typeparam name="T1"> Parameter Type </typeparam>
    /// <typeparam name="C1"> Callback Type </typeparam>
    /// <param name="url"> API URL</param>
    /// <param name="parameter"> Parameter with Type T1</param>
    /// <param name="callback"> Callback with Type C1 </param>
    /// <returns></returns>

    public IEnumerator UserManagementCallAPI<T1, C1>(string url, T1 parameter, Action<C1> callback, GameObject activityIndicator = null, string authorization = "") where C1 : new()
    {
        string jsonData = JsonUtility.ToJson(parameter);

        Debug.Log(jsonData);

        using (UnityWebRequest webRequest = UnityWebRequest.Post(url, jsonData))
        {
            webRequest.SetRequestHeader("Accept", "application/json");
            webRequest.SetRequestHeader("Content-Type", "application/json");
            webRequest.timeout = WEB_REQUEST_TIMEOUT;

            if (authorization != null)
            {
                webRequest.SetRequestHeader("Authorization", $"Bearer {authorization}");
            }

            webRequest.uploadHandler = new UploadHandlerRaw(System.Text.Encoding.UTF8.GetBytes(jsonData));

            if (activityIndicator != null)
            {
                activityIndicator.SetActive(true);
            }

            yield return webRequest.SendWebRequest();

            if (webRequest.isNetworkError)
            {
                LogRequest(webRequest);

                C1 c = new C1();
                ObjectExtensions.SetPropertyValue(c, "ResponseCode", -1);

                callback(c);
                if (activityIndicator != null)
                {
                    activityIndicator.SetActive(false);
                }
            }
            else
            {
                if (webRequest.isDone)
                {
                    LogRequest(webRequest);

                    string jsonFile = System.Text.Encoding.UTF8.GetString(webRequest.downloadHandler.data);
                    var userData = JsonUtility.FromJson<C1>(jsonFile);

                    int value = (int)webRequest.responseCode;
                    ObjectExtensions.SetPropertyValue(userData, "ResponseCode", value);
                    callback(userData);
                    if (activityIndicator != null)
                    {
                        activityIndicator.SetActive(false);
                    }
                }
            }
        }
    }

    public IEnumerator Get(string url, Action<int, string,string> callback, Action<List<Reward>> returnRewards, string authorization, GameObject activityIndicator)
    {
        using (UnityWebRequest webRequest = UnityWebRequest.Get(url))
        {
            webRequest.SetRequestHeader("Accept", "application/json");
            webRequest.SetRequestHeader("Authorization", $"Bearer {authorization}");
            webRequest.timeout = WEB_REQUEST_TIMEOUT;

            if (activityIndicator != null)
            {
                activityIndicator.SetActive(true);
            }

            yield return webRequest.SendWebRequest();

            if (webRequest.isNetworkError)
            {
                LogRequest(webRequest);

                if (activityIndicator != null)
                {
                    activityIndicator.SetActive(false);
                }

                callback(-1, "Error", webRequest.error);
            }
            else
            {
                if (webRequest.isDone)
                {
                    LogRequest(webRequest);

                    string jsonFile = System.Text.Encoding.UTF8.GetString(webRequest.downloadHandler.data);
                    RewardsData reward = JsonUtility.FromJson<RewardsData>(jsonFile);
                    returnRewards(reward.rewards);

                    if (activityIndicator != null)
                    {
                        activityIndicator.SetActive(false);
                    }

                    callback((int)webRequest.responseCode, "","");
                }
            }
        }
    }

    public IEnumerator DownloadImage(string MediaUrl, Action<Sprite> callback, Sprite defualtImage)
    {
        using (UnityWebRequest request = UnityWebRequestTexture.GetTexture(MediaUrl))
        {
            yield return request.SendWebRequest();

            if (request.isNetworkError)
            {
                LogRequest(request);
                callback(defualtImage);
            }
            else
            {
                if (request.isDone)
                {
                    LogRequest(request);
                    Texture2D tex = (((DownloadHandlerTexture)request.downloadHandler).texture != null) ? ((DownloadHandlerTexture)request.downloadHandler).texture : null;

                    if (tex == null)
                        callback(defualtImage);
                    else
                        callback(Sprite.Create(tex, new Rect(0f, 0f, tex.width, tex.height), new Vector2(.5f, .5f), 100f));

                }
            }
        }
    }

    public IEnumerator UploadImage(string url, byte[] texturedata, Action callback, Action<UpdateProfilePictureResponse> response, string authorization, GameObject activityIndicator)
    {

        string data = Convert.ToBase64String(texturedata);

        List<IMultipartFormSection> fileform = new List<IMultipartFormSection>();
        fileform.Add(new MultipartFormFileSection("image", texturedata, "_profilepic.jpg", "image/jpg"));

        WWWForm form = new WWWForm();
        form.AddField("image", data);

        using (UnityWebRequest webRequest = UnityWebRequest.Post(url, form))
        {
            webRequest.SetRequestHeader("Accept", "application/json");
            webRequest.SetRequestHeader("Content-Type", "application/json");
            webRequest.SetRequestHeader("Authorization", $"Bearer {authorization}");
            webRequest.uploadHandler = new UploadHandlerRaw(texturedata);

            if (activityIndicator != null)
            {
                activityIndicator.SetActive(true);
            }

            yield return webRequest.SendWebRequest();

            if (webRequest.isNetworkError)
            {
                LogRequest(webRequest);
                UpdateProfilePictureResponse baseResponse = new UpdateProfilePictureResponse
                {
                    ResponseCode = (int)webRequest.responseCode,
                    message = webRequest.error,
                    title = "Error"
                };
                response(baseResponse);
                if (activityIndicator != null)
                {
                    activityIndicator.SetActive(false);
                }
            }
            else
            {
                if (webRequest.isDone)
                {
                    string jsonFile = System.Text.Encoding.UTF8.GetString(webRequest.downloadHandler.data);
                    var userData = JsonUtility.FromJson<UpdateProfilePictureResponse>(jsonFile);

                    response(userData);

                    LogRequest(webRequest);

                    callback();

                    if (activityIndicator != null)
                    {
                        activityIndicator.SetActive(false);
                    }
                }
            }
        }
    }


    private void LogRequest(UnityWebRequest request)
    {
        Debug.Log($"Response Code: {request.responseCode}\nError: {request.error}");
    }
}
public static class JsonHelper
{
    public static T[] FromJson<T>(string json)
    {
        Wrapper<T> wrapper = JsonUtility.FromJson<Wrapper<T>>(json);
        return wrapper.Items;
    }

    public static string ToJson<T>(T[] array)
    {
        Wrapper<T> wrapper = new Wrapper<T>();
        wrapper.Items = array;
        return JsonUtility.ToJson(wrapper);
    }

    public static string ToJson<T>(T[] array, bool prettyPrint)
    {
        Wrapper<T> wrapper = new Wrapper<T>();
        wrapper.Items = array;
        return JsonUtility.ToJson(wrapper, prettyPrint);
    }

    [System.Serializable]
    public class Wrapper<T>
    {
        public T[] Items;
    }
}

public static class ObjectExtensions
{
    public static void SetPropertyValue<T>(this object obj, string propertyName, T propertyValue) where T : IConvertible
    {
        PropertyInfo pi = obj.GetType().GetProperty(propertyName);

        if (pi != null && pi.CanWrite)
        {
            pi.SetValue(obj, Convert.ChangeType(propertyValue, pi.PropertyType));
            ;
        }
    }
}

