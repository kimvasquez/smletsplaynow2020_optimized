﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System.IO;


[System.Serializable]
public class BaseResponse
{
    public string message;
    public string errors;
    public string title;
    public int ResponseCode { get; set; }
}
/****************************SERIALIZED JSON****************************/


#region Return_Data
/*--------------LOGIN-------------*/
[System.Serializable]

public class UserData: BaseResponse
{
    public string email;
    public string first_name;
    public string last_name;
    public string mobile_number;
    public string accessToken;
    public string image_url;
    public string verified;

   
}
/*--------------REGISTER-------------*/
[System.Serializable]

public class RegisterUserData: BaseResponse
{
    public string first_name;
    public string last_name;
    public string email;
    public string accessToken;
    public string image_url;

}

/*--------------ForgotPass-------------*/

public class ForgotPassData : BaseResponse
{
    public string email;
  
}
/*--------------VerifyData-------------*/
[System.Serializable]
public class VerifyData : BaseResponse
{
    public string email;
}


[System.Serializable]
public class CheckAppVersionData : BaseResponse
{
    public string live_ios;
    public string development_ios;
    public string live_android;
    public string development_android;
}

/*--------------Change Password-------------*/

public class ChangePasswordData : BaseResponse
{
 
}

/*--------------Scan-------------*/
[System.Serializable]

public class ScanMarker : BaseResponse
{
    public string coins;
    public string close;
    public string success;
    public string location;

}

/*--------------REWARDS LIST-------------*/
[System.Serializable]
public class Reward
{
    public string code;
    public string mall_name;
    public string title;
    public string description;
    public string mechanics;
    public string image;
    public string valid_until;
    public int is_claimed;
    public string type;
    public string claim_date;
}

[System.Serializable]
public class RewardsData
{
    public string title;
    public string message;
    public List<Reward> rewards;
}

/*--------------UPLOADPIC-------------*/
[System.Serializable]
public class UpdateProfilePictureResponse : BaseResponse
{
    public string image_url;
}
#endregion
/****************************REQUEST PARAMETER****************************/
#region Parameter
/*--------------FORGOT-------------*/

[System.Serializable]
public class ForgotPassParams
{
    public string email;
}
/*--------------REGISTER-------------*/

[System.Serializable]

public class RegisterParams
{
    public string first_name;
    public string last_name;
    public string email;
    public string mobile_number;
    public string device_id;
    public string password;
    public string confirm_password;
}

/*--------------LOGIN-------------*/
[System.Serializable]
public class LoginParams
{
    public string email;
    public string device_id;
    public string password;
}

/*--------------Verify-------------*/
[System.Serializable]
public class VerifyParams
{
    public string email;
}


/*--------------CheckAppVersion-------------*/
[System.Serializable]
public class CheckAppVersionParams
{
    public string platform;
}

/*--------------CHANGE PASSWORD-------------*/
public class ChangePasswordParam
{
    public string current_password;
    public string new_password;
    public string confirm_new_password;
}

/*--------------SCAN-------------*/
public class ScanMarkerParams
{
    public string longitude;
    public string latitude;
    public string marker_id;
}

[System.Serializable]
public class UpdateProfilePicture
{
    public string image;
}
#endregion