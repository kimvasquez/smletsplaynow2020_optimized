﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using TMPro;
using System.IO;
using System;
using System.Text;
using System.Runtime.Serialization.Formatters.Binary;

public class ProfileControler : BaseController
{
    public TextMeshProUGUI UserFullName;
    public Image UserImage;
    public Image buttonPic;

    public RewardsController rewardsController;

    public Sprite defualtProfile;

    public Sprite downloadedProfilePic;

    private void Awake()
    {
        if (UserDataSessionManager.Instance.AccessToken != null)
        {
            UpdateProfile();
        }
    }

    public void UpdateProfile()
    {
        if (downloadedProfilePic == null)
        {
            StartCoroutine(RestfulAPI.Instance.DownloadImage(UserDataSessionManager.Instance.ImageURL, SetImage, defualtProfile));
        }
        UserFullName.text = $"{UserDataSessionManager.Instance.UserFirstName} {UserDataSessionManager.Instance.UserLastName}";
    }

    public void Logout()
    {
        UIManager.Instance.DialogBox.CreateNewDialog(
            DialogType.QUESTION, 
            "Logout", 
            "Are you sure you want to logout?", 
            "Logout", 
            delegate
            {
                UserDataSessionManager.Instance.ClearUserData();
                UIManager.Instance.ProfilePanel.SetActive(false);
                UIManager.Instance.Marker.SetActive(false);
                UIManager.Instance.BackgroundCanvas.SetActive(true);
                UIManager.Instance.InitialCamera.SetActive(true);
                UIManager.Instance.ARCamera.SetActive(false);
                UIManager.Instance.InitialCanvas.SetActive(true);
                UIManager.Instance.Splash2.SetActive(true);
                UIManager.Instance.ARCanvas.SetActive(false);
                downloadedProfilePic = null;
                FirebaseChecker.Instance.LogFirebaseEvent("Login");

                UIManager.Instance.behaviour.enabled = false;
                UIManager.Instance.behaviour2.enabled = false;
                UIManager.Instance.behaviour3.enabled = false;
                UIManager.Instance.behaviour4.enabled = false;
                UIManager.Instance.behaviour5.enabled = false;
                UIManager.Instance.santaBehaviour.enabled = false;
                UIManager.Instance.santaBehaviour2.enabled = false;
                UIManager.Instance.santaBehaviour3.enabled = false;
                UIManager.Instance.santaBehaviour4.enabled = false;
                UIManager.Instance.santaBehaviour5.enabled = false;
                UIManager.Instance.santaPose1.SetActive(false);
                UIManager.Instance.santaPose2.SetActive(false);
                UIManager.Instance.santaPose3.SetActive(false);
                UIManager.Instance.santaPose4.SetActive(false);
                UIManager.Instance.santaPose5.SetActive(false);
                UIManager.Instance.Settings.SetActive(false);
                UIManager.Instance.OnBoardingScreen.SetActive(false);
                UIManager.Instance.Splash2.SetActive(true);
            }, 
            true);
        UIManager.Instance.DialogBox.ShowDialog();
    }

    void SetImage(Sprite image)
    {
        downloadedProfilePic = image;

        if (downloadedProfilePic != null)
        {

            buttonPic.sprite = downloadedProfilePic;
            UserImage.sprite = downloadedProfilePic;
        }
    }

    public void UploadProfilePic()
    {
        FirebaseChecker.Instance.LogFirebaseEvent("UpdateProfilePic");
#if UNITY_EDITOR

        if (defualtProfile == null)
        {
            Debug.Log("Couldn't load texture");
            return;
        }

        UserImage.sprite = Sprite.Create(defualtProfile.texture, new Rect(0f, 0f, defualtProfile.texture.width, defualtProfile.texture.height), new Vector2(.5f, .5f), 100f);

        string encodedText1 = Convert.ToBase64String(defualtProfile.texture.EncodeToPNG());

        Debug.Log("method 1 base 64 string: " + encodedText1);

        UpdateProfilePicture profilePicture1 = new UpdateProfilePicture {
            image = encodedText1
        };
        

        StartCoroutine(RestfulAPI.Instance.UserManagementCallAPI<UpdateProfilePicture, UpdateProfilePictureResponse>(AppConstants.BASE_URL + AppConstants.UPLOAD_URL, profilePicture1, Response, UIManager.Instance.LoadingIndicator, UserDataSessionManager.Instance.AccessToken));


#else

        if (!UIManager.Instance.CheckGalleryPermission())
        {
            return;
        }
       

        NativeGallery.Permission permission = NativeGallery.GetImageFromGallery((path) =>
        {
            Debug.Log("Image path: " + path);
            if (path != null)
            {

                Texture2D tex = null;
                byte[] fileData = null;

                if (File.Exists(path))
                {
                    fileData = File.ReadAllBytes(path);
                    tex = new Texture2D(2, 2);
                    tex.LoadImage(fileData);
                }
                else
                {
                    return;
                }

                SetImage(Sprite.Create(tex, new Rect(0f, 0f, tex.width, tex.height), new Vector2(.5f, .5f), 100f));

                string encodedText = Convert.ToBase64String(fileData);

                Debug.Log("method 1 base 64 string: " + encodedText);

                UpdateProfilePicture profilePicture = new UpdateProfilePicture {
                image = encodedText
                };

                StartCoroutine(RestfulAPI.Instance.UserManagementCallAPI<UpdateProfilePicture, UpdateProfilePictureResponse>(AppConstants.BASE_URL + AppConstants.UPLOAD_URL, profilePicture, Response, UIManager.Instance.LoadingIndicator, UserDataSessionManager.Instance.AccessToken));
        /*
                Destroy(texture);

                StartCoroutine(RestfulAPI.Instance.UploadImage(AppConstants.BASE_URL + AppConstants.UPLOAD_URL, , delegate
                {
                    UpdateProfile();
                },
                Response
                ,
                UserDataSessionManager.Instance.AccessToken,
                UIManager.Instance.LoadingIndicator));
        */
            }
        }, "Select an image", "image/png");
#endif


    }

    private void Response(UpdateProfilePictureResponse baseResponse)
    {
        if (CheckResponseCode(baseResponse.ResponseCode, baseResponse.title, baseResponse.message))
        {
            Debug.Log(baseResponse.image_url);
            UserDataSessionManager.Instance.ImageURL = baseResponse.image_url;
        }
    }  
}
