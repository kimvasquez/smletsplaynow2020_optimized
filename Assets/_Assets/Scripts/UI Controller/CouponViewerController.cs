﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using TMPro;
using UnityEngine.UI;
using ZXing.Common;
using ZXing;
using ZXing.QrCode;
using System.Text.RegularExpressions;

public class CouponViewerController : MonoBehaviour
{
    [Header("Coupon")]
    [Space(20)]
    public GameObject ViewCouponPanel;
    public TextMeshProUGUI txtCouponTitle;
    public TextMeshProUGUI txtCouponMall;
    public TextMeshProUGUI txtCouponMechanics;
    public TextMeshProUGUI txtCouponValidDate;
    public TextMeshProUGUI txtCouponClaimedDate;
    public TextMeshProUGUI txtCouponCode;

    public RawImage QRCode;
    public Image imgCouponImage;

    public GameObject redeemedPanel;
    public GameObject toRedeemPanel;

    public GameObject btnHowToRedeem;

    public Sprite PlaceHolder;


    public Sprite defualtProduct;

    public void ViewCoupon(Reward coupon)
    {
        imgCouponImage.sprite = PlaceHolder;
        StartCoroutine(RestfulAPI.Instance.DownloadImage(coupon.image, SetImage, defualtProduct));
        txtCouponTitle.text = coupon.title;
        txtCouponMall.text = coupon.mall_name;
        txtCouponMechanics.text = Regex.Replace(coupon.mechanics, @"<[^>]*>", string.Empty);

        if (coupon.is_claimed == 0)
        {
            toRedeemPanel.SetActive(true);
            redeemedPanel.SetActive(false);
            txtCouponValidDate.text = $"Valid until {ConvertDate(coupon.valid_until)}";
            txtCouponCode.text = coupon.code;
            btnHowToRedeem.SetActive(true);
            QRCode.texture = generateQR(coupon.code);
        }
        else
        {
            redeemedPanel.SetActive(true);
            toRedeemPanel.SetActive(false);
            btnHowToRedeem.SetActive(false);
            txtCouponClaimedDate.text = $"Claimed last {ConvertDate(coupon.claim_date)}";


        }
        ViewCouponPanel.SetActive(true);
    }

    public Texture2D generateQR(string text)
    {
        var encoded = new Texture2D(256, 256);

        IBarcodeWriter barcodeWriter = new BarcodeWriter();
        QRCodeWriter qRCodeWriter = new QRCodeWriter();
        Color32Renderer color = new Color32Renderer();

        var color32 = color.Render(qRCodeWriter.encode(text, BarcodeFormat.QR_CODE, encoded.width, encoded.height), BarcodeFormat.QR_CODE, text);
        
        encoded.SetPixels32(color32);
        encoded.Apply();
        return encoded;
    }

    void SetImage(Sprite image)
    {

        imgCouponImage.sprite = image;
    }

    string ConvertDate(string numericDate)
    {
        string[] separator = {"-"};
        string[] tempDate = numericDate.Split(separator, 3, System.StringSplitOptions.RemoveEmptyEntries);

        if (tempDate.Length != 3)
        {
            return numericDate;
        }
        
        return $"{GetMonth(tempDate[1])} {GetDay(tempDate[2])[0]}, {tempDate[0]} {GetDay(tempDate[2])[1]}";
    }

    string GetMonth(string numericMonth)
    {
        switch (int.Parse(numericMonth))
        {
            case 1:
                return "January";
            case 2:
                return "February";
            case 3:
                return "March";
            case 4:
                return "April";
            case 5:
                return "May";
            case 6:
                return "June";
            case 7:
                return "July";
            case 8:
                return "August";
            case 9:
                return "September";
            case 10:
                return "October";
            case 11:
                return "November";
            case 12:
                return "December";
            default:
                return "December";
        }
    }

    string[] GetDay(string numericDate)
    {
        string[] separator = { " " };
        string[] tempTime = numericDate.Split(separator, 2, System.StringSplitOptions.RemoveEmptyEntries);

        if (tempTime.Length != 2)
        {
            return new string[] {numericDate, ""};
        }

        return new string[] { $"{int.Parse(tempTime[0])}", GetTime(tempTime[1])} ;
    }

    string GetTime(string time)
    {
        string[] separator = { ":" };
        string[] tempTime = time.Split(separator, 3, System.StringSplitOptions.RemoveEmptyEntries);

        if (tempTime.Length != 3)
        {
            return "";
        }

        int tempHour = int.Parse(tempTime[0]);
        int realhour = (tempHour > 12) ? tempHour - 12 : tempHour;
        string ampm = (tempHour > 12) ? "PM" : "AM";
        return $"{realhour}:{tempTime[1]} {ampm}";
    }
}
