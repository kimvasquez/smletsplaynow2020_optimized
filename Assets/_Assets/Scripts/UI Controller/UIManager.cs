﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using TMPro;
using Vuforia;
using UnityEngine.EventSystems;
using UnityEngine.Video;
#if UNITY_ANDROID
using UnityEngine.Android;
#endif


public class UIManager : MonoBehaviour
{
    private static UIManager _instance;
    public static UIManager Instance
    {
        get
        {
            if (_instance == null)
            {
                _instance = FindObjectOfType<UIManager>();
                if (_instance == null)
                {
                    GameObject go = new GameObject();
                    go.name = typeof(UIManager).Name;
                    _instance = go.AddComponent<UIManager>();
                    DontDestroyOnLoad(go);
                }
            }
            return _instance;
        }
    }

    public TextMeshProUGUI versionNumber;
    public DialogBox DialogBox;
    public DialogText DialogText;
    public GameObject LoadingIndicator;

    public GameObject ARCanvas;
    public GameObject InitialCanvas;
    public GameObject BackgroundCanvas;

    public GameObject ARCamera;
    public GameObject InitialCamera;

    public CouponViewerController couponViewer;

    public GameObject Marker;

    public GameObject ProfilePanel;

    public GameObject OneTimeScreen;
    public GameObject Splash2;

    public GameObject Splash1;

    public ImageTargetBehaviour behaviour;
    public ImageTargetBehaviour behaviour2;
    public ImageTargetBehaviour behaviour3;
    public ImageTargetBehaviour behaviour4;
    public ImageTargetBehaviour behaviour5;
    public ImageTargetBehaviour santaBehaviour;
    public ImageTargetBehaviour santaBehaviour2;
    public ImageTargetBehaviour santaBehaviour3;
    public ImageTargetBehaviour santaBehaviour4;
    public ImageTargetBehaviour santaBehaviour5;
    public int markerID;
    public int santaMarkerID;

    public MarkerBehaviour markerBehaviour1;
    public MarkerBehaviour markerBehaviour2;
    public MarkerBehaviour markerBehaviour3;
    public MarkerBehaviour markerBehaviour4;
    public MarkerBehaviour markerBehaviour5;

    public SantaMarkerBehaviour santamarkerBehaviour;

    public TabGroup tabGroup;
    public TabButton initialTabButton;
    public AudioSource musicPlayer;
    public UnityEngine.UI.Image santaPoseSilhouette;

    public UnityEngine.UI.RawImage screenshotPreview;
    public GameObject CaptureScreen;
    public GameObject PreviewScreen;


    public VideoPlayer santaPoseVid1;
    public VideoPlayer santaPoseVid2;
    public VideoPlayer santaPoseVid3;
    public VideoPlayer santaPoseVid4;
    public VideoPlayer santaPoseVid5;
    public GameObject santaPose1;
    public GameObject santaPose2;
    public GameObject santaPose3;
    public GameObject santaPose4;
    public GameObject santaPose5;
    public GameObject santaPoseContainer;
    public GameObject Settings;

    public GameObject OnBoardingScreen;
    private void Awake()
    {
        FirebaseChecker.Instance.LogFirebaseEvent("SplashScreen");
        Splash1.SetActive(true);
        Application.targetFrameRate = 60;
        Screen.fullScreen = true;
        Debug.Log(UserDataSessionManager.Instance.AccessToken);
        versionNumber.text = "Version " + Application.version;
#if UNITY_ANDROID && !UNITY_EDITOR
        if (IsEmulator())
        {
            FirebaseChecker.Instance.LogFirebaseEvent("UsingEmulator");
            DialogBox.CreateNewDialog(
                DialogType.ERROR,
                "ERROR",
                "It seems that you are using an emulator. The application will now quit.",
                "QUIT",
                delegate 
                {
                    Application.Quit();
                });
            DialogBox.ShowDialog();
            return;
        }
#endif
        if (UserDataSessionManager.Instance.AccessToken != "")
        {
            ARCanvas.SetActive(true);
            ARCamera.SetActive(true);
            InitialCamera.SetActive(false);
            Marker.SetActive(true);
            
            if (ARCanvas.activeSelf)
            {
                tabGroup.OnTabSelected(initialTabButton);
            }

            santaBehaviour.enabled = true;
            santaBehaviour2.enabled = true;
            santaBehaviour3.enabled = true;
            santaBehaviour4.enabled = true;
            santaBehaviour5.enabled = true;
            behaviour.enabled = false;
            behaviour2.enabled = false;
            behaviour3.enabled = false;
            behaviour4.enabled = false;
            behaviour5.enabled = false;
            santamarkerBehaviour.enabled = true;
            santaPoseContainer.SetActive(true);
            StartCoroutine(ImageTargetDelayOff());
            FirebaseChecker.Instance.LogFirebaseEvent("Santa_AR_View");
        }
        else
        {
#if UNITY_ANDROID && !UNITY_EDITOR
            CheckLocationPermission();
#endif
            StartCoroutine(SantaTargetDelayOff());
            InitialCanvas.SetActive(true);
            FirebaseChecker.Instance.LogFirebaseEvent("Login");
            Splash2.SetActive(true);
            BackgroundCanvas.SetActive(true);
        }
    }



#if UNITY_ANDROID && !UNITY_EDITOR
    public void CheckLocationPermission()
    {
        AndroidRuntimePermissions.Permission result = AndroidRuntimePermissions.RequestPermission(Permission.FineLocation);

        if (result == AndroidRuntimePermissions.Permission.ShouldAsk)
        {
            DialogBox.CreateNewDialog(
                DialogType.ERROR,
                "LOCATION IS NOT ENABLED",
                "The application needs your permission to access the phone's location in order to function properly.",
                "Okay",
                delegate { AndroidRuntimePermissions.RequestPermission(Permission.FineLocation); });
            DialogBox.ShowDialog();
            
        }
        else if (result == AndroidRuntimePermissions.Permission.Denied)
        {
            DialogBox.CreateNewDialog(
               DialogType.ERROR,
               "LOCATION IS NOT ENABLED",
               "The application needs your permission to access the phone's location in order to function properly.",
               "Open Settings",
               delegate { AndroidRuntimePermissions.OpenSettings(); });
            DialogBox.ShowDialog();
          
        }
       
    }
#endif
    IEnumerator SantaTargetDelayOff()
    {

        yield return new WaitForSeconds(2f);
        santaPoseContainer.SetActive(false);
        santaPose1.SetActive(false);
        santaPose2.SetActive(false);
        santaPose3.SetActive(false);
        santaPose4.SetActive(false);
        santaPose5.SetActive(false);

    }
        IEnumerator ImageTargetDelayOff()
    {

        yield return new WaitForSeconds(2f);
        behaviour.gameObject.SetActive(false);
        behaviour2.gameObject.SetActive(false);
        behaviour3.gameObject.SetActive(false);
        behaviour4.gameObject.SetActive(false);
        behaviour5.gameObject.SetActive(false);
        behaviour.enabled = false;
        behaviour2.enabled = false;
        behaviour3.enabled = false;
        behaviour4.enabled = false;
        behaviour5.enabled = false;


        santaPose1.SetActive(false);
        santaPose2.SetActive(false);
        santaPose3.SetActive(false);
        santaPose4.SetActive(false);
        santaPose5.SetActive(false);

    }

    public void CloseCaptureScreen()
    {
        StartCoroutine(CloseCapture());
    }

    IEnumerator CloseCapture()
    {
        yield return new WaitForSeconds(0.5f);
        UIManager.Instance.santaBehaviour.enabled = true;
        UIManager.Instance.santaBehaviour2.enabled = true;
        UIManager.Instance.santaBehaviour3.enabled = true;
        UIManager.Instance.santaBehaviour4.enabled = true;
        UIManager.Instance.santaBehaviour5.enabled = true;
    }

    public void RetakePhoto()
    {
        InitialCamera.SetActive(false);
        BackgroundCanvas.SetActive(false);
        ARCamera.SetActive(true);
        Marker.SetActive(true);
        santaPoseContainer.SetActive(true);
        CaptureScreen.SetActive(false);
    }

    public void SantaMarkerID(int _santaId)
    {
        santaMarkerID = _santaId;
    }

    public bool CheckGalleryWritePermission()
    {
#if UNITY_ANDROID && !UNITY_EDITOR
        AndroidRuntimePermissions.Permission result = AndroidRuntimePermissions.RequestPermission(Permission.ExternalStorageWrite);

        if (result == AndroidRuntimePermissions.Permission.ShouldAsk)
        {
            DialogBox.CreateNewDialog(
                DialogType.ERROR,
                "CAN'T ACCESS GALLERY",
                "The application needs your permission to access the phone's gallery in order to continue.",
                "Okay",
                delegate { AndroidRuntimePermissions.RequestPermission(Permission.ExternalStorageWrite); });
            DialogBox.ShowDialog();
            return false;
        }
        else if (result == AndroidRuntimePermissions.Permission.Denied)
        {
            DialogBox.CreateNewDialog(
               DialogType.ERROR,
               "CAN'T ACCESS GALLERY",
               "The application needs your permission to access the phone's gallery in order to continue.",
               "Open Settings",
               delegate { AndroidRuntimePermissions.OpenSettings(); });
            DialogBox.ShowDialog();
            return false;
        }
        else
        {
            return true;
        }


#else
        return true;
#endif
    }

    public bool CheckGalleryPermission()
    {
#if UNITY_ANDROID && !UNITY_EDITOR
        AndroidRuntimePermissions.Permission result = AndroidRuntimePermissions.RequestPermission(Permission.ExternalStorageRead);

        if (result == AndroidRuntimePermissions.Permission.ShouldAsk)
        {
            DialogBox.CreateNewDialog(
                DialogType.ERROR,
                "CAN'T ACCESS GALLERY",
                "The application needs your permission to access the phone's gallery in order to continue.",
                "Okay",
                delegate { AndroidRuntimePermissions.RequestPermission(Permission.ExternalStorageRead); });
            DialogBox.ShowDialog();
            return false;
        }
        else if (result == AndroidRuntimePermissions.Permission.Denied)
        {
            DialogBox.CreateNewDialog(
               DialogType.ERROR,
               "CAN'T ACCESS GALLERY",
               "The application needs your permission to access the phone's gallery in order to continue.",
               "Open Settings",
               delegate { AndroidRuntimePermissions.OpenSettings(); });
            DialogBox.ShowDialog();
            return false;
        }
        else
        {
            return true;
        }


#else
        return true;
#endif

    }

    public void InitializedVuforiaBehaviour()
    {
        VuforiaBehaviour.Instance.enabled = true;
    }
    public void DeInitializedVuforiaBehaviour()
    {
        VuforiaBehaviour.Instance.enabled = false;
    }
    public void SetOnBoardingDone()
    {
        PlayerPrefs.SetInt("OnBoardingDone", 1);
        FirebaseChecker.Instance.LogFirebaseEvent("Santa_AR_View");

        InitialCanvas.SetActive(false);
        Instance.BackgroundCanvas.SetActive(false);
        Instance.ARCanvas.SetActive(true);

        Instance.ARCamera.SetActive(true);
        Instance.Marker.SetActive(true);
        Instance.InitialCamera.SetActive(false);
        Instance.OneTimeScreen.SetActive(false);
        Instance.tabGroup.OnTabSelected(initialTabButton);

        Instance.santaBehaviour.enabled = true;
        Instance.santaBehaviour2.enabled = true;
        Instance.santaBehaviour3.enabled = true;
        Instance.santaBehaviour4.enabled = true;
        Instance.santaBehaviour5.enabled = true;
        Instance.behaviour.enabled = false;
        Instance.behaviour2.enabled = false;
        Instance.behaviour3.enabled = false;
        Instance.behaviour4.enabled = false;
        Instance.behaviour5.enabled = false;
    }

   

    public void OpenCoupon(Reward coupon)
    {
        couponViewer.ViewCoupon(coupon);
        FirebaseChecker.Instance.LogFirebaseEvent("CouponViewer");
    }

    public Sprite[] CouponTypes;
    public Sprite GetCouponType(string couponType)
    {
        if (couponType == "tangible")
        {
            return CouponTypes[0];
        }
        else
        {
            return CouponTypes[1];
        }
    }
    public void EnableMusicPLayer()
    {
        musicPlayer.gameObject.SetActive(true);
        musicPlayer.Play();
        
    }

    public void DisableMusicPlayer()
    {
        musicPlayer.Stop();
        musicPlayer.gameObject.SetActive(false);
    }

    public void EnableMarkers()
    {
        if (!ProfilePanel.activeSelf && ARCanvas.activeSelf)
        {
            Marker.SetActive(true);
        }
    }

#if UNITY_ANDROID && !UNITY_EDITOR
    public bool IsEmulator()
    {

        AndroidJavaClass osBuild = new AndroidJavaClass("android.os.Build");

        if (osBuild.GetStatic<string>("BRAND") == null)
        {
            return true;
        }

        return (osBuild.GetStatic<string>("BRAND").Contains("generic") && osBuild.GetStatic<string>("DEVICE").Contains("generic"))
            || osBuild.GetStatic<string>("FINGERPRINT").Contains("generic")
            || osBuild.GetStatic<string>("FINGERPRINT").Contains("unknown")
            || osBuild.GetStatic<string>("HARDWARE").Contains("goldfish")
            || osBuild.GetStatic<string>("HARDWARE").Contains("ranchu")
            || osBuild.GetStatic<string>("MODEL").Contains("google_sdk")
            || osBuild.GetStatic<string>("MODEL").Contains("Emulator")
            || osBuild.GetStatic<string>("MODEL").Contains("Android SDK built for x86")
            || osBuild.GetStatic<string>("MANUFACTURER").Contains("Genymotion")
            || osBuild.GetStatic<string>("PRODUCT").Contains("sdk_google")
            || osBuild.GetStatic<string>("PRODUCT").Contains("google_sdk")
            || osBuild.GetStatic<string>("PRODUCT").Contains("sdk")
            || osBuild.GetStatic<string>("PRODUCT").Contains("sdk_x86")
            || osBuild.GetStatic<string>("PRODUCT").Contains("vbox86p")
            || osBuild.GetStatic<string>("PRODUCT").Contains("emulator")
            || osBuild.GetStatic<string>("PRODUCT").Contains("simulator");
    }
#endif

    public void ChangeScreenMode(bool mode)
    {
#if !UNITY_ANDROID
        Screen.fullScreen = mode;
#endif
    }


#if UNITY_ANDROID
    TouchScreenKeyboard keyboard;

    private void Update()
    {
        if (Input.GetKeyDown(KeyCode.Escape))
        {
            if (keyboard != null)
            {
                keyboard.active = false;
            }
        }

        if (Input.GetMouseButtonDown(0))
        {
            if (EventSystem.current.currentSelectedGameObject == null)
            {
                Debug.Log("Hey nothing is selected");
                if (keyboard != null)
                {
                    keyboard.active = false;
                }
            }
        }
    }
#endif
}
