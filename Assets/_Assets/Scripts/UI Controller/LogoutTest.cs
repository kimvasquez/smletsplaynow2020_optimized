﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class LogoutTest : MonoBehaviour
{
    public void LogoutTesting()
    {
        UIManager.Instance.DialogBox.CreateNewDialog(
            DialogType.QUESTION,
            "Logout",
            "Are you sure you want to logout?",
            "Logout",
            delegate
            {
                UserDataSessionManager.Instance.ClearUserData();
                UIManager.Instance.ProfilePanel.SetActive(false);
                UIManager.Instance.Marker.SetActive(false);
                UIManager.Instance.BackgroundCanvas.SetActive(true);
                UIManager.Instance.InitialCamera.SetActive(true);
                UIManager.Instance.ARCamera.SetActive(false);
                UIManager.Instance.InitialCanvas.SetActive(true);
                UIManager.Instance.Splash2.SetActive(true);
                UIManager.Instance.ARCanvas.SetActive(false);
                FirebaseChecker.Instance.LogFirebaseEvent("Register/Login");

                UIManager.Instance.santaBehaviour.enabled = false;
                UIManager.Instance.santaBehaviour2.enabled = false;
                UIManager.Instance.santaBehaviour3.enabled = false;
                UIManager.Instance.santaBehaviour4.enabled = false;
                UIManager.Instance.santaBehaviour5.enabled = false;
                UIManager.Instance.behaviour.enabled = false;
                UIManager.Instance.behaviour2.enabled = false;
                UIManager.Instance.behaviour3.enabled = false;
                UIManager.Instance.behaviour4.enabled = false;
                UIManager.Instance.behaviour5.enabled = false;
            },
            true);
        UIManager.Instance.DialogBox.ShowDialog();
    }
}
