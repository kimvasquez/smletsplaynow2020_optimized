﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using TMPro;

public class DialogBox : MonoBehaviour
{
    [Space(50)]
    [Header("Success = 0, Error = 1, CloseMall = 2, LocationMall = 3, CoinsWin = 4, Generic = 5, Question = 6")]
    public Sprite[] dialogSprites;
    public GameObject dialogWindow;
    public Image dialogImage;
    public TextMeshProUGUI dialogTitle;
    public TextMeshProUGUI dialogMessage;
    public TextMeshProUGUI dialogButtonText;
    public Button dialogButton;
    public GameObject exitButton;

    public void CreateNewDialog(DialogType dialogType, string DialogTitleToDisplay, string DialogMessageToDisplay, string DialogButtonTextToDisplay, System.Action callback = null, bool showExit = false)
    {
        dialogImage.sprite = dialogSprites[(int)dialogType];
        dialogTitle.text = DialogTitleToDisplay;
        dialogMessage.text = DialogMessageToDisplay;
        dialogButtonText.text = DialogButtonTextToDisplay;

        exitButton.SetActive(showExit);

        if (callback != null)
        {
            dialogButton.onClick.AddListener(delegate {
                callback();
                dialogButton.onClick.RemoveAllListeners();
            });
        }
    }

    public void ShowDialog()
    {
        dialogWindow.SetActive(true);
    }
}
public enum DialogType
{
    SUCCESS = 0,
    ERROR = 1,
    CLOSE_MALL= 2,
    LOCATION_MALL = 3,
    COINS_WIN = 4,
    GENERIC_ICON = 5,
    QUESTION = 6
}
