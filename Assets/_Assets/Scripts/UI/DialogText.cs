﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using TMPro;
public class DialogText : MonoBehaviour
{
    [Header("LOGIN ERROR FIELD")]
    [Space(10)]
    public TextMeshProUGUI txtErrorLoginDialog;

    public void CreateErrorLoginDialog(string errorDialog)
    {
        txtErrorLoginDialog.text = errorDialog;
    }
}
