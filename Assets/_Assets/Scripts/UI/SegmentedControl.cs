﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using TMPro;

public class SegmentedControl : MonoBehaviour
{
    public System.Action<int> onValueChange;

    private Button leftButton;
    private Button rightButton;

    private Image leftButtonBG;
    private Image rightButtonBG;

    private TextMeshProUGUI leftButtonText;
    private TextMeshProUGUI rightButtonText;

    public Color primaryColor;
    public Color secondaryColor;

    private bool isLeftSelected = true;

    private void Awake()
    {

        leftButton = transform.GetChild(0).GetComponent<Button>();
        rightButton = transform.GetChild(1).GetComponent<Button>();

        leftButtonText = leftButton.transform.GetChild(0).GetComponent<TextMeshProUGUI>();
        rightButtonText = rightButton.transform.GetChild(0).GetComponent<TextMeshProUGUI>();

        leftButtonBG = leftButton.GetComponent<Image>();
        rightButtonBG = rightButton.GetComponent<Image>();

        leftButton.onClick.AddListener(delegate
        {
            if (!isLeftSelected)
            {
                onValueChange?.Invoke(0);
                isLeftSelected = true;
            }

            leftButtonBG.color = primaryColor;
            rightButtonText.color = primaryColor;

            leftButtonText.color = secondaryColor;
            rightButtonBG.color = secondaryColor;
        });

        rightButton.onClick.AddListener(delegate
        {
            if (isLeftSelected)
            {
                onValueChange?.Invoke(1);
                isLeftSelected = false;
            }
            leftButtonBG.color = secondaryColor;
            rightButtonText.color = secondaryColor;

            leftButtonText.color = primaryColor;
            rightButtonBG.color = primaryColor;
        });
    }

    public void ResetSegment()
    {
        leftButtonBG.color = primaryColor;
        rightButtonText.color = primaryColor;

        leftButtonText.color = secondaryColor;
        rightButtonBG.color = secondaryColor;
    }

    public void ClickCoupon()
    {
        leftButton.onClick.Invoke();
    }
}
