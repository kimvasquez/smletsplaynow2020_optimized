﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class AutoDisable : MonoBehaviour
{
    public float SecondsBeforeDisabling = 1f;

    private void OnEnable()
    {
        StartCoroutine(DisableGO());
    }

    IEnumerator DisableGO()
    {
        yield return new WaitForSecondsRealtime(SecondsBeforeDisabling);
        gameObject.SetActive(false);
    }
}
