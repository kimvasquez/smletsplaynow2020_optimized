﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class FadeInOut : MonoBehaviour {

    private Image m_image;

    [Space(10)]
    [Header("Timing")]
    public float delay = 0;
    public float duration = 1;
    public bool retainAlpha = false;


    [Space(10)]
    [Header("Transition Curve")]
    public AnimationCurve colorCurve;

    private Color originalColor;

    bool isPreviouslyDisabled = false;


    private void Awake()
    {
        m_image = GetComponent<Image>();
        originalColor = m_image.color;
    }

    private void Start()
    {
        StartCoroutine(Fade());
    }

    private void OnEnable()
    {
        if (isPreviouslyDisabled)
        {
            StartCoroutine(Fade());

            isPreviouslyDisabled = false;
        }
    }

    private void OnDisable()
    {
        isPreviouslyDisabled = true;
    }

    IEnumerator Fade()
    {
        m_image.color = originalColor;
        yield return new WaitForSecondsRealtime(delay);

        float percent = 0;

        do
        {
            percent += Time.deltaTime/duration;

            if (retainAlpha)
            {
                if (colorCurve.Evaluate(percent) < originalColor.a)
                {
                    m_image.color = new Color(originalColor.r, originalColor.g, originalColor.b, colorCurve.Evaluate(percent));
                }
                else
                {
                    m_image.color = originalColor;
                }
            }
            else
            {
                m_image.color = new Color(originalColor.r, originalColor.g, originalColor.b, colorCurve.Evaluate(percent));
            }
            

            yield return null;
        } while (percent < 1);

        if (percent == 1)
        {
            gameObject.SetActive(false);
        }
    }
}
