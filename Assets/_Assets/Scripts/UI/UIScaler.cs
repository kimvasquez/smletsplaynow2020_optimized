﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class UIScaler : MonoBehaviour
{
    public Vector2 EndScale;

    public RectTransform rectTransform;

    private void OnEnable()
    {
        Debug.Log((rectTransform.rect.height / rectTransform.rect.width));
        if ((rectTransform.rect.height / rectTransform.rect.width) <= 1.4f)
        {
            GetComponent<RectTransform>().localScale = EndScale;
        }
        else
        {
            GetComponent<RectTransform>().localScale = new Vector2(1f,1f) ;
        }
    }
}
