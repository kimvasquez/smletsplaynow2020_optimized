﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using TMPro;

public class TMPFadeInOut : MonoBehaviour
{

    private TextMeshProUGUI m_text;

    [Space(10)]
    [Header("Timing")]
    public float delay = 0;
    public float duration = 1;

    [Space(10)]
    [Header("Transition Curve")]
    public AnimationCurve colorCurve;

    private Color originalColor;

    bool isPreviouslyDisabled = false;


    private void Awake()
    {
        m_text = GetComponent<TextMeshProUGUI>();
        originalColor = m_text.color;
    }

    private void Start()
    {
        StartCoroutine(Fade());
    }

    private void OnEnable()
    {
        if (isPreviouslyDisabled)
        {
            StartCoroutine(Fade());
            isPreviouslyDisabled = false;
        }
    }

    private void OnDisable()
    {
        isPreviouslyDisabled = true;
    }

    IEnumerator Fade()
    {
        m_text.color = originalColor;
        yield return new WaitForSecondsRealtime(delay);

        float percent = 0;

        do
        {
            percent += Time.deltaTime / duration;
            m_text.color = new Color(originalColor.r, originalColor.g, originalColor.b, colorCurve.Evaluate(percent));

            yield return null;
        } while (percent < 1);
    }
}
