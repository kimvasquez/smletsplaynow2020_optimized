﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class AnimatedUIEntry : MonoBehaviour
{

    public static bool isAnimating;

    [Header("Switches")]
    [Space(10)]
    public bool animateOnStart = false;
    public bool enableOnExit = false;
    public bool useEntryAnimationForExit = false;
    public GameObject objectToDisableOnExit;

    [Header("Timing")]
    [Space(10)]
    public float delay = 0;
    public float duration = 1;
    public float exitDuaration = 1;

    [Header("Scale")]
    [Header("ENTRY")]
    [Space(10)]
    public Vector2 entryEndingScale;
    public Vector2 entryStartingScale;
    public AnimationCurve entryScaleCurve;

    [Header("Position")]
    [Space(10)]
    public Vector2 entryEndingPos;
    public Vector2 entryStartingPos;
    public AnimationCurve entryPosCurve;


    [Header("Scale")]
    [Header("EXIT")]
    [Space(10)]
    public Vector2 exitEndingScale;
    public Vector2 exitStartingScale;
    public AnimationCurve exitScaleCurve;

    [Header("Position")]
    [Space(10)]
    public Vector2 exitEndingPos;
    public Vector2 exitStartingPos;
    public AnimationCurve exitPosCurve;

    RectTransform mRectTransform;

    private void Awake()
    {
        mRectTransform = GetComponent<RectTransform>();
    }

    private void OnEnable()
    {
        if (animateOnStart)
        {
            StartCoroutine(Animation(1));
        }
    }

    public void CallEntryAnimation(int multiplier)
    {
        StartCoroutine(Animation(multiplier));
    }

    IEnumerator Animation(int value)
    {
        mRectTransform.anchoredPosition = entryStartingPos;
        mRectTransform.localScale = entryStartingScale;

        Vector2 entryPos = new Vector2((entryStartingPos.x * value), entryStartingPos.y);

        yield return new WaitForSecondsRealtime(delay);

        float time = 0;
        float percent = 0;
        float lastTime = Time.realtimeSinceStartup;

        do
        {
            time += Time.realtimeSinceStartup - lastTime;
            lastTime = Time.realtimeSinceStartup;
            percent = Mathf.Clamp01(time / duration);

            Vector2 tempScale = Vector2.LerpUnclamped(entryStartingScale, entryEndingScale, entryScaleCurve.Evaluate(percent));

            Vector2 tempPos = Vector2.LerpUnclamped(entryPos, entryEndingPos, entryPosCurve.Evaluate(percent));

            mRectTransform.anchoredPosition = tempPos;
            mRectTransform.localScale = tempScale;

            isAnimating = true;

            yield return null;
        } while (percent < 1);

        mRectTransform.localScale = entryEndingScale;
        mRectTransform.anchoredPosition = entryEndingPos;
        isAnimating = false;
    }

    public void CallExitAnimation(int multiplier)
    {
        StartCoroutine(ExitAnimation(multiplier));
    }

    IEnumerator ExitAnimation(int value)
    {
        Vector2 endingScale = (useEntryAnimationForExit) ? entryStartingScale : exitEndingScale;
        Vector2 endingPos = (useEntryAnimationForExit) ? entryStartingPos : exitEndingPos;

        Vector2 startingPos = (useEntryAnimationForExit) ? entryEndingPos : exitStartingPos;
        Vector2 startingScale = (useEntryAnimationForExit) ? entryEndingScale : exitStartingScale;

        endingPos.x = endingPos.x * value;

        AnimationCurve scaleCurve = (useEntryAnimationForExit) ? entryScaleCurve : exitScaleCurve;
        AnimationCurve posCurve = (useEntryAnimationForExit) ? entryPosCurve : exitPosCurve;

        mRectTransform.anchoredPosition = endingPos;
        mRectTransform.localScale = entryStartingPos;

        float time = 0;
        float percent = 0;
        float lastTime = Time.realtimeSinceStartup;

        do
        {
            time += Time.realtimeSinceStartup - lastTime;
            lastTime = Time.realtimeSinceStartup;
            percent = Mathf.Clamp01(time / exitDuaration);

            Vector2 tempScale = Vector2.LerpUnclamped(startingScale, endingScale, scaleCurve.Evaluate(percent));

            Vector2 tempPos = Vector2.LerpUnclamped(startingPos, endingPos, posCurve.Evaluate(percent));

            mRectTransform.anchoredPosition = tempPos;
            mRectTransform.localScale = tempScale;

            isAnimating = true;

            yield return null;
        } while (percent < 1);

        mRectTransform.localScale = endingScale;
        mRectTransform.anchoredPosition = endingPos;
        isAnimating = false;
        objectToDisableOnExit.SetActive(enableOnExit);
    }
}
