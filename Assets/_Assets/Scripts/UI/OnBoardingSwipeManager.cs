﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class OnBoardingSwipeManager : MonoBehaviour
{
    public DanielLochner.Assets.SimpleScrollSnap.SimpleScrollSnap SimpleScrollSnap;
    bool isLoading = false;
    public GameObject OnBoarding;
    public GameObject nextButton;
    public GameObject skipButton;

    void Start()
    {
        SimpleScrollSnap.onPanelSelected.AddListener(OnPageChanging);
        SimpleScrollSnap.onPanelChanged.AddListener(OnPageChanged);
    }

    private void OnPageChanged()
    {
        FirebaseChecker.Instance.LogFirebaseEvent($"Santa_On_Boarding_{SimpleScrollSnap.CurrentPanel + 1}");
    }

    private void OnPageChanging()
    {
        
        if (SimpleScrollSnap.TargetPanel == SimpleScrollSnap.NumberOfPanels - 1)
        {
            //SimpleScrollSnap.pagination.SetActive(false);
            nextButton.SetActive(false);
            skipButton.SetActive(false);
        }
        else
        {
            SimpleScrollSnap.pagination.SetActive(true);
            nextButton.SetActive(true);
            skipButton.SetActive(true);
        }
    }



    void Update()
    {
        if (isLoading)
        {
            return;
        }

        if (!OnBoarding.activeSelf)
        {
            return;
        }

        if (SwipeControls.Instance.SwipeRight)
        {
            if (SimpleScrollSnap.CurrentPanel < SimpleScrollSnap.NumberOfPanels)
            {
                if (SimpleScrollSnap.CurrentPanel == 0)
                {
                    return;
                }
                SimpleScrollSnap.GoToPreviousPanel();
            }
        }

        if (SwipeControls.Instance.SwipeLeft)
        {
            if (SimpleScrollSnap.CurrentPanel > -1)
            {
                if (SimpleScrollSnap.CurrentPanel == SimpleScrollSnap.NumberOfPanels)
                {
                    return;
                }
                SimpleScrollSnap.GoToNextPanel();
            }
        }
    }
}
