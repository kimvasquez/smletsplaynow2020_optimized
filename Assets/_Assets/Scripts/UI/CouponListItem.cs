﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using TMPro;
using UnityEngine.UI;

public class CouponListItem : MonoBehaviour
{
    public Reward reward;

    public void UpdateData()
    {
        transform.GetChild(0).GetComponent<TextMeshProUGUI>().text = reward.title;
        transform.GetChild(1).GetComponent<TextMeshProUGUI>().text = reward.valid_until;
        transform.GetChild(2).GetComponent<TextMeshProUGUI>().text = reward.mall_name;
        transform.GetChild(3).GetComponent<Image>().sprite = UIManager.Instance.GetCouponType(reward.type);
    }

    public void OpenCouponPage()
    {
        UIManager.Instance.OpenCoupon(reward);
    }
}
