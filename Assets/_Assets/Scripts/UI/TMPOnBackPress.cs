﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using TMPro;
using System;

public class TMPOnBackPress : MonoBehaviour
{
    private TMP_InputField input;

    private void Awake()
    {
        input = GetComponent<TMP_InputField>();
        input.onEndEdit.AddListener(EndEdit);
        input.onValueChanged.AddListener(Editing);
        input.onTouchScreenKeyboardStatusChanged.AddListener(ReportChangeStatus);
    }


    bool keepOldTextInField;
    string editText;
    string oldEditText;

    private void ReportChangeStatus(TouchScreenKeyboard.Status newStatus)
    {
        if (newStatus == TouchScreenKeyboard.Status.Canceled)
            keepOldTextInField = true;
    }

    private void Editing(string currentText)
    {
        oldEditText = editText;
        editText = currentText;
    }

    private void EndEdit(string currentText)
    {
        if (keepOldTextInField && !string.IsNullOrEmpty(oldEditText))
        {
            //IMPORTANT ORDER
            editText = oldEditText;
            input.text = oldEditText;

            keepOldTextInField = false;
        }
    }
}
