﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class ImageFitter : MonoBehaviour
{
    private RectTransform parent;
    private RectTransform child;
    private AspectRatioFitter aspectRatioFitter;
    private Sprite currentImage;

    private void OnGUI()
    {

        parent = transform.parent.GetComponent<RectTransform>();
        child = GetComponent<RectTransform>();
        currentImage = GetComponent<Image>().sprite;
        aspectRatioFitter = GetComponent<AspectRatioFitter>();

        Vector2 imageSize = new Vector2(currentImage.texture.width, currentImage.texture.height);

        bool isWidthLarger = (currentImage.texture.width > currentImage.texture.height) ? true : false;

        aspectRatioFitter.aspectRatio = (isWidthLarger) ? 2f : .5f;
    }
}
