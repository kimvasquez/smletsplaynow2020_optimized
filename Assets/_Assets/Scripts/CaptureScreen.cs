﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Android;

public class CaptureScreen : MonoBehaviour
{

    public RectTransform rectT;
    public GameObject captureButton;
    public AnimatedUIEntry previewCaptureScreen;

    int width;
    int height;
    private void Start()
    {
        width = System.Convert.ToInt32(rectT.rect.width);
        height = System.Convert.ToInt32(rectT.rect.height);
    }

    private void OnEnable()
    {
        UIManager.Instance.santaBehaviour.enabled = false;
        UIManager.Instance.santaBehaviour2.enabled = false;
        UIManager.Instance.santaBehaviour3.enabled = false;
        UIManager.Instance.santaBehaviour4.enabled = false;
        UIManager.Instance.santaBehaviour5.enabled = false;
        captureButton.SetActive(true);
    }



    public void TakePhoto()
    {
        StartCoroutine(ScreenShot());
    }

    Texture2D imageToShare;
    IEnumerator ScreenShot()
    {
        captureButton.SetActive(false);
        imageToShare = null;
        yield return new WaitForEndOfFrame();

        Vector3[] corners = new Vector3[4];
        rectT.GetWorldCorners(corners);
        var startX = corners[0].x;
        var startY = corners[0].y;

        width = (int)corners[3].x - (int)corners[0].x;
        height = (int)corners[2].y - (int)corners[0].y;

        imageToShare = new Texture2D(width, height, TextureFormat.RGB24, false);
        imageToShare.ReadPixels(new Rect(startX, startY, width, height), 0, 0);
        imageToShare.Apply();

        FirebaseChecker.Instance.LogFirebaseEvent("Santa_Photo_Preview");

        UIManager.Instance.santaPoseVid1.Stop();
        UIManager.Instance.santaPoseVid2.Stop();
        UIManager.Instance.santaPoseVid3.Stop();
        UIManager.Instance.santaPoseVid4.Stop();
        UIManager.Instance.santaPoseVid5.Stop();
        UIManager.Instance.santaPose1.SetActive(false);
        UIManager.Instance.santaPose2.SetActive(false);
        UIManager.Instance.santaPose3.SetActive(false);
        UIManager.Instance.santaPose4.SetActive(false);
        UIManager.Instance.santaPose5.SetActive(false);


        UIManager.Instance.screenshotPreview.texture = imageToShare;
        previewCaptureScreen.transform.parent.gameObject.SetActive(true);
        previewCaptureScreen.CallEntryAnimation(1);
        

        UIManager.Instance.InitialCamera.SetActive(true);
        UIManager.Instance.BackgroundCanvas.SetActive(true);
        UIManager.Instance.ARCamera.SetActive(false);
        UIManager.Instance.Marker.SetActive(false);
        UIManager.Instance.santaPoseContainer.SetActive(false);
        UIManager.Instance.CaptureScreen.SetActive(false);


    }

}
