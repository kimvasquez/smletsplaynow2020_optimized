﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using TMPro;

public class ChangePasswordController : BaseController
{
    [Header("CHANGE PASSWORD")]
    [Space(10)]
    public TMP_InputField txt_changePass_current;
    public TMP_InputField txt_changePass_new;
    public TMP_InputField txt_changePass_confirmNew;

    public AnimatedUIEntry ChangePass;

    private void ChangePasswordCallBack(ChangePasswordData changePasswordData)
    {
        if (CheckResponseCode(changePasswordData.ResponseCode, changePasswordData.title, changePasswordData.message))
        {
            UIManager.Instance.DialogBox.CreateNewDialog(DialogType.SUCCESS, 
                "SUCCESS!", 
                "Password has been successfully changed.", 
                "OK",
                delegate
                {
                    ChangePass.CallExitAnimation(1);
                    ResetInput();
                }
                );
            UIManager.Instance.DialogBox.ShowDialog();
            ResetInput();
        }
    }

    public void ResetInput()
    {
        txt_changePass_confirmNew.text = txt_changePass_current.text = txt_changePass_new.text = "";
    }

    public void ChangePassword()
    {
        if (txt_changePass_current.text.Length < 8 || txt_changePass_new.text.Length < 8 || txt_changePass_confirmNew.text.Length < 8)
        {
            UIManager.Instance.DialogBox.CreateNewDialog(DialogType.ERROR, "ERROR!", "Password must contain at least 8 characters. Please check and try again.", "OK");
            UIManager.Instance.DialogBox.ShowDialog();
        }
        else if (txt_changePass_new.text != txt_changePass_confirmNew.text)
        {
            UIManager.Instance.DialogBox.CreateNewDialog(DialogType.ERROR, "ERROR!", "Password do not match.", "OK");
            UIManager.Instance.DialogBox.ShowDialog();
        }
        else
        {
            ChangePasswordParam changePasswordParam = new ChangePasswordParam
            {
                current_password = txt_changePass_current.text.TrimEnd('\u200b'),
                new_password = txt_changePass_new.text.TrimEnd('\u200b'),
                confirm_new_password = txt_changePass_confirmNew.text.TrimEnd('\u200b')
            };
            StartCoroutine(RestfulAPI.Instance.UserManagementCallAPI<ChangePasswordParam, ChangePasswordData>(AppConstants.BASE_URL + AppConstants.CHANGE_PASS_URL, changePasswordParam, ChangePasswordCallBack, UIManager.Instance.LoadingIndicator, UserDataSessionManager.Instance.AccessToken));

        }
    }
}
