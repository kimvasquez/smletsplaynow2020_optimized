﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using TMPro;
using UnityEngine.UI;

public class RegisterController : BaseController
{
    [Header("REGISTER FIELD")]
    [Space(10)]
    public TMP_InputField txtReg_email;
    public TMP_InputField txtReg_fname;
    public TMP_InputField txtReg_lname;
    public TMP_InputField txtReg_mobile;
    public TMP_InputField txtReg_address;
    public TMP_InputField txtReg_password;
    public TMP_InputField txtReg_confirmPass;

    public TextMeshProUGUI txtRegError_email;
    public TextMeshProUGUI txtRegError_fname;
    public TextMeshProUGUI txtRegError_lname;
    public TextMeshProUGUI txtRegError_mobile;
    public TextMeshProUGUI txtRegError_address;
    public TextMeshProUGUI txtRegError_password;
    public TextMeshProUGUI txtRegError_confirmPass;
    public TextMeshProUGUI txtRegError_tosandpolicy;

    public Toggle isAgreed;

    public AnimatedUIEntry RegisterPanel;
    public AnimatedUIEntry loginPanel;
    public AnimatedUIEntry regPanelSuccess;
    public GameObject regBackButton;
    public ScrollRect regScrollRect;
    public float RegScrollVertPosition;
    private void RegisterCallback(RegisterUserData registerUserData)
    {
        if (CheckResponseCode(registerUserData.ResponseCode, registerUserData.title, registerUserData.message))
        {

            RegisterPanel.CallExitAnimation(1);
            //LoginPanel.CallEntryAnimation(1);
            regPanelSuccess.transform.parent.gameObject.SetActive(true);
            regPanelSuccess.CallEntryAnimation(-1);
            ResetInput();
            ResetRegErrorValue();
            FirebaseChecker.Instance.LogFirebaseEvent("Registration_Success");

        }
    }

    public void ResetInput()
    {
        txtReg_email.text = txtReg_fname.text = txtReg_lname.text = txtReg_lname.text = txtReg_mobile.text = txtReg_address.text = txtReg_confirmPass.text = txtReg_password.text = "";
        isAgreed.isOn = false;
    }

    public void Register()
    {
        if (ValidateInput())
        {
            RegisterParams registerParams = new RegisterParams
            {
                email = txtReg_email.text.TrimEnd('\u200b'),
                first_name = txtReg_fname.text.TrimEnd('\u200b'),
                last_name = txtReg_lname.text.TrimEnd('\u200b'),
                mobile_number = txtReg_mobile.text.TrimEnd('\u200b'),
                device_id = DeviceType(),
                password = txtReg_password.text.TrimEnd('\u200b'),
                confirm_password = txtReg_confirmPass.text.TrimEnd('\u200b')
            };

            StartCoroutine(RestfulAPI.Instance.UserManagementCallAPI<RegisterParams, RegisterUserData>(AppConstants.BASE_URL + AppConstants.REGISTER_URL, registerParams, RegisterCallback, UIManager.Instance.LoadingIndicator));
            PlayerPrefs.SetString("RegEmail", txtReg_email.text.TrimEnd('\u200b'));
            PlayerPrefs.Save();
        }
    }

    public bool ValidateInput()
    {

        if (txtReg_email.text.Length <= 1 || txtReg_fname.text.Length <= 1 ||
            txtReg_lname.text.Length <= 1 || txtReg_mobile.text.Length < 1 ||
            (txtReg_password.text != txtReg_confirmPass.text) ||
            txtReg_password.text.Length < 8 || txtReg_confirmPass.text.Length < 8 ||
            isAgreed.isOn.Equals(false) || txtReg_email.text.Length <= 1 || 
            txtReg_mobile.text.Length > 11 || txtReg_mobile.text.Length < 11)
        {
            if (txtReg_fname.text.Length <= 1)
            {
                txtRegError_fname.text = "Please fill up the first name field correctly.";
                txtRegError_fname.gameObject.SetActive(true);

            }

            if (txtReg_lname.text.Length <= 1)
            {
                txtRegError_lname.text = "Please fill up the last name field correctly.";
                txtRegError_lname.gameObject.SetActive(true);
            }

            if (txtReg_email.text.Length <= 1)
            {
                txtRegError_email.text = "Please fill up the Email field correctly.";
                txtRegError_email.gameObject.SetActive(true);
            }

            if (txtReg_mobile.text.Length < 1 || txtReg_mobile.text.Length > 11 || txtReg_mobile.text.Length < 11)
            {
                txtRegError_mobile.text = "Please check your mobile number, should be 11 digits.";
                txtRegError_mobile.gameObject.SetActive(true);
            }


            if (txtReg_confirmPass.text.Length < 8)
            {
                txtRegError_confirmPass.text = "Password must be 8 characters length";
                txtRegError_confirmPass.gameObject.SetActive(true);
                //return false;
            }
            else if (txtReg_confirmPass.text.Length <= 1)
            {
                txtRegError_confirmPass.text = "Please fill up the confirm field correctly.";
                txtRegError_confirmPass.gameObject.SetActive(true);
            }

            if (txtReg_password.text.Length < 8)
            {
                txtRegError_password.text = "Password must be 8 characters length";
                txtRegError_password.gameObject.SetActive(true);
                //return false;
            }
            else if (txtReg_password.text != txtReg_confirmPass.text)
            {
                txtRegError_password.text = "Password do not match";
                txtRegError_password.gameObject.SetActive(true);
                txtRegError_confirmPass.text = "Password do not match";
                txtRegError_confirmPass.gameObject.SetActive(true);
                //return false;
            }
            else if (txtReg_password.text.Length <= 1)
            {
                txtRegError_password.text = "Please fill up the password field correctly.";
                txtRegError_password.gameObject.SetActive(true);
            }

            if (isAgreed.isOn.Equals(false))
            {
                txtRegError_tosandpolicy.text = "You must agree to the terms condition and privacy policy to create an account";
                txtRegError_tosandpolicy.gameObject.SetActive(true);
                //return false;
            }

            return false;
        }
        else
        {

            return true;
        }
    }

    private void Update()
    {
        
        if(regScrollRect.verticalNormalizedPosition > RegScrollVertPosition)
        {
            regBackButton.SetActive(true);
        }
        else
        {
            regBackButton.SetActive(false);
        }
    }

    public void ResetRegErrorValue()
    {
        txtRegError_email.text = "";
        txtRegError_fname.text = "";
        txtRegError_lname.text = "";
        txtRegError_mobile.text = "";
        txtRegError_address.text = "";
        txtRegError_password.text = "";
        txtRegError_confirmPass.text = "";
        txtRegError_email.gameObject.SetActive(false);
        txtRegError_fname.gameObject.SetActive(false);
        txtRegError_lname.gameObject.SetActive(false);
        txtRegError_mobile.gameObject.SetActive(false);
        txtRegError_address.gameObject.SetActive(false);
        txtRegError_password.gameObject.SetActive(false);
        txtRegError_confirmPass.gameObject.SetActive(false);
    }

    public void FirstNameDeselect()
    {
        if (txtReg_fname.text.Length <= 1)
        {
            txtRegError_fname.text = "Please fill up the first name field correctly.";
            txtRegError_fname.gameObject.SetActive(true);

        }
    }
    public void FirstNameOnValueChange()
    {
        txtRegError_fname.text = "";
        txtRegError_fname.gameObject.SetActive(false);
    }

    public void LastNameDeselect()
    {
        if (txtReg_lname.text.Length <= 1)
        {
            txtRegError_lname.text = "Please fill up the last name field correctly.";
            txtRegError_lname.gameObject.SetActive(true);
        }
    }
    public void lastNameOnValueChange()
    {
        txtRegError_lname.text = "";
        txtRegError_lname.gameObject.SetActive(false);
    }

    public void EmailDeselect()
    {
        if (txtReg_email.text.Length <= 1)
        {
            txtRegError_email.text = "Please fill up the Email Address field correctly.";
            txtRegError_email.gameObject.SetActive(true);
        }
    }
    public void EmailOnValueChange()
    {
        txtRegError_email.text = "";
        txtRegError_email.gameObject.SetActive(false);
    }

    public void MobileDeselect()
    {
        if (txtReg_mobile.text.Length < 1 || txtReg_mobile.text.Length > 11 || txtReg_mobile.text.Length < 11)
        {
            txtRegError_mobile.text = "Please check your mobile number, should be 11 digits.";
            txtRegError_mobile.gameObject.SetActive(true);
        }

    }
    public void MobileOnValueChange()
    {
        txtRegError_mobile.text = "";
        txtRegError_mobile.gameObject.SetActive(false);
    }

    public void AddressDeselect()
    {

    }
    public void AddressOnValueChange()
    {
        txtRegError_address.gameObject.SetActive(false);
    }

    public void PasswordDeselect()
    {
        if (txtReg_password.text.Length <= 1)
        {
            txtRegError_password.text = "Please fill up the password field correctly.";
            txtRegError_password.gameObject.SetActive(true);
        }
    }
    public void PasswordOnValueChange()
    {
        txtRegError_password.text = "";
        txtRegError_password.gameObject.SetActive(false);
    }

    public void ConfirmPassDeselect()
    {
        if (txtReg_confirmPass.text.Length <= 1)
        {
            txtRegError_confirmPass.text = "Please fill up the confirm field correctly.";
            txtRegError_confirmPass.gameObject.SetActive(true);
        }
    }
    public void ConfirmPassOnValueChange()
    {
        txtRegError_confirmPass.text = "";
        txtRegError_confirmPass.gameObject.SetActive(false);
    }
}
