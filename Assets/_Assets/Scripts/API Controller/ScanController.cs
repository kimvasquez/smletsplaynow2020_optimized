﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ScanController : BaseController
{

    double strLat;
    double strLong;

    public void OnBoxTap(System.Action onTapCallback)
    {
#if !UNITY_EDITOR
        if (!LocationService.Instance.isLocationServiceAvailable || !Input.location.isEnabledByUser)
        {
            UIManager.Instance.DialogBox.CreateNewDialog(DialogType.ERROR, "UNABLE TO GET LOCATION", "Please make sure that your location is turned on in order to use the app.", "OK");
            UIManager.Instance.DialogBox.ShowDialog();
            LocationService.Instance.RestartLocationService();
            onTapCallback();
            return;
        }

        strLong = LocationService.Instance.longitude;
        strLat = LocationService.Instance.latitude;

        if (strLat != 0 || strLong != 0)
        {
            SendScan();
            onTapCallback();
            Debug.Log("Tapped the box");
        }
        else
        {
            UIManager.Instance.DialogBox.CreateNewDialog(DialogType.ERROR, "UNABLE TO GET LOCATION", "Please make sure that your location is turned on in order to use the app.", "OK");
            UIManager.Instance.DialogBox.ShowDialog();
            LocationService.Instance.RestartLocationService();
            onTapCallback();
            return;
        }
#endif

        onTapCallback();
    }

    private void ScanCallback(ScanMarker scanMarker)
    {
        Debug.Log("Response: " + scanMarker.ResponseCode + " Message: " + scanMarker.message);
        if (CheckResponseCodeScan(scanMarker.ResponseCode, scanMarker.title, scanMarker.message, scanMarker.location, scanMarker.close, scanMarker.coins,scanMarker.success))
        {

        }
    }
    public  bool CheckResponseCodeScan(int responseCode, string title, string message, string location, string close, string coins, string isSuccess)
    {
        
        switch (responseCode)
        {
            case 200:
                if(coins == "0")
                {
                    UIManager.Instance.DialogBox.CreateNewDialog(DialogType.GENERIC_ICON, "OOPS!", message, "OK");
                    UIManager.Instance.DialogBox.ShowDialog();
                }
                if ( coins == "1" ) {
                    UIManager.Instance.DialogBox.CreateNewDialog(DialogType.COINS_WIN, title, message, "OK");
                    UIManager.Instance.DialogBox.ShowDialog();
                }
                return true;
            case -1:
                UIManager.Instance.DialogBox.CreateNewDialog(DialogType.ERROR, "WE CAN'T DETECT YOUR INTERNET CONNECTION", "Please check your internet connecton strength and try again.", "OK");
                UIManager.Instance.DialogBox.ShowDialog();
                return false;
            case 500:
                UIManager.Instance.DialogBox.CreateNewDialog(DialogType.ERROR, title, message, "OK");
                UIManager.Instance.DialogBox.ShowDialog();
                return false;
            case 422:
                if(location == "true")
                {
                    UIManager.Instance.DialogBox.CreateNewDialog(DialogType.LOCATION_MALL, title, message, "OK");
                    UIManager.Instance.DialogBox.ShowDialog();
                }
                if(close == "true")
                {
                    UIManager.Instance.DialogBox.CreateNewDialog(DialogType.CLOSE_MALL, title, message, "OK");
                    UIManager.Instance.DialogBox.ShowDialog();
                }
                //if(isSuccess == "false")
                //{
                //    UIManager.Instance.DialogBox.CreateNewDialog(DialogType.GENERIC_ICON, "OOPS!", "Please try again to win exciting prizes!", "OK");
                //    UIManager.Instance.DialogBox.ShowDialog();
                //}
                UIManager.Instance.DialogBox.CreateNewDialog(DialogType.GENERIC_ICON, "OOPS!", "Please try again to win exciting prizes!", "OK");
                UIManager.Instance.DialogBox.ShowDialog();

                return false;
            case 402:
                UIManager.Instance.DialogBox.CreateNewDialog(DialogType.ERROR, title, message, "OK");
                UIManager.Instance.DialogBox.ShowDialog();
                return false;
            case 401:
                UIManager.Instance.DialogBox.CreateNewDialog(
                    DialogType.ERROR,
                    "Session Expired",
                    "You are already log in to another device.",
                    "OK",
                    delegate {
                        UserDataSessionManager.Instance.ClearUserData();
                        UIManager.Instance.Marker.SetActive(false);
                        UIManager.Instance.BackgroundCanvas.SetActive(true);
                        UIManager.Instance.InitialCamera.SetActive(true);
                        UIManager.Instance.ARCamera.SetActive(false);
                        UIManager.Instance.InitialCanvas.SetActive(true);
                        UIManager.Instance.Splash2.SetActive(true);
                        UIManager.Instance.ARCanvas.SetActive(false);
                    });
                UIManager.Instance.DialogBox.ShowDialog();
                return false;
            default:
                UIManager.Instance.DialogBox.CreateNewDialog(DialogType.ERROR, "ERROR", "Something went wrong. Please check and try again.", "OK");
                UIManager.Instance.DialogBox.ShowDialog();
                return false;
        }
    }
        public void SendScan()
    {

        if (strLat != 0 || strLong != 0)
        {
            ScanMarkerParams scanMarkerParams = new ScanMarkerParams
            {
                longitude = "" + strLong,
                latitude = "" + strLat,
                marker_id = UIManager.Instance.markerID.ToString()
            };

            string accessToken = UserDataSessionManager.Instance.AccessToken;
            StartCoroutine(RestfulAPI.Instance.UserManagementCallAPI<ScanMarkerParams, ScanMarker>(AppConstants.BASE_URL + AppConstants.SCANS_URL, scanMarkerParams, ScanCallback, UIManager.Instance.LoadingIndicator, UserDataSessionManager.Instance.AccessToken));
        }
        else
        {
            UIManager.Instance.DialogBox.CreateNewDialog(DialogType.LOCATION_MALL, "ERROR!", "We can't get your location please check and try again", "OK");
            UIManager.Instance.DialogBox.ShowDialog();
        }

    }
}
