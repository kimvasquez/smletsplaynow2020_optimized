﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using TMPro;
using System.Linq;

public class RewardsController : BaseController
{
    public GameObject ListItem;
    public Transform ListParent;

    public GameObject EmptyRewardList;
    public GameObject EmptyClaimedList;
    public GameObject RewardList;

    public SegmentedControl segment;
    private int segmentValue;

    public GameObject ProfilePanel;
    public MarkerBehaviour marker;

    private void OnEnable()
    {
        segment.onValueChange += OnSegmentValueChange;
        segmentValue = 0;
    }

    private void OnDisable()
    {
        segment.onValueChange -= OnSegmentValueChange;
    }

    void OnSegmentValueChange(int value)
    {
        segmentValue = value;
        GetRewardList();
    }

    private void RewardListCallBack(List<Reward> rewardListData)
    {
        if (rewardListData.Count == 0)
        {
            EmptyRewardList.SetActive(true);
            RewardList.SetActive(false);
         
        }
        else
        {
            List<Reward> tempRewards = new List<Reward>();
            foreach (Reward reward in rewardListData)
            {
                if (reward.is_claimed == segmentValue)
                {
                    tempRewards.Add(reward);
                }
            }

            if (segmentValue == 0)
                FirebaseChecker.Instance.LogFirebaseEvent("UnclaimedList");
            else
                FirebaseChecker.Instance.LogFirebaseEvent("ClaimedList");

            if (tempRewards.Count == 0)
            {
                EmptyRewardList.SetActive(true);
                RewardList.SetActive(false);
                return;
            }

            EmptyClaimedList.SetActive(false);
            EmptyRewardList.SetActive(false);
            RewardList.SetActive(true);

            foreach (Reward item in tempRewards)
            {
                if (item.is_claimed != segmentValue)
                {
                    return;
                }
                CouponListItem coupon = Instantiate(ListItem, ListParent).GetComponent<CouponListItem>();
                coupon.reward = item;
                coupon.UpdateData();
            }
        }
    }

    public void DestroyAllContent()
    {
        for (int index = 0; index < ListParent.childCount; index++)
        {
            Destroy(ListParent.GetChild(index).gameObject);
        }
    }

    public void CheckResponse(int _responsecode, string _title, string _message)
    {
        if (CheckResponseCode(_responsecode, _title, _message))
        {
            if (!ProfilePanel.activeSelf)
            {
                ProfilePanel.SetActive(true);
                FirebaseChecker.Instance.LogFirebaseEvent("Profile");
                UIManager.Instance.Marker.SetActive(false);
                UIManager.Instance.santaBehaviour.enabled = false;
                UIManager.Instance.santaBehaviour2.enabled = false;
                UIManager.Instance.santaBehaviour3.enabled = false;
                UIManager.Instance.santaBehaviour4.enabled = false;
                UIManager.Instance.santaBehaviour5.enabled = false;
                UIManager.Instance.behaviour.enabled = false;
                UIManager.Instance.behaviour2.enabled = false;
                UIManager.Instance.behaviour3.enabled = false;
                UIManager.Instance.behaviour4.enabled = false;
                UIManager.Instance.behaviour5.enabled = false;
                marker.musicPlayer.Stop();
            }
        }
        else
        {
            UIManager.Instance.Marker.SetActive(true);
        }
    }

    public void GetRewardList()
    {
        DestroyAllContent();
        StartCoroutine(RestfulAPI.Instance.Get(AppConstants.BASE_URL + AppConstants.REWARDS_URL, CheckResponse, RewardListCallBack, UserDataSessionManager.Instance.AccessToken, UIManager.Instance.LoadingIndicator));
    }
}
