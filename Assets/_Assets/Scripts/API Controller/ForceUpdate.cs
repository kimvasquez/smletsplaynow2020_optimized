﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ForceUpdate : BaseController
{
    public GameObject forceUpdateScreen;
    public string androidURL;
    public string iosURL;
    private void Start()
    {
        CheckAppVersion();
    }

    public void CheckAppVersionCallBack(CheckAppVersionData data)
    {
        if (CheckAppVerResponseCode(data.ResponseCode, data.title, data.message))
        {
#if UNITY_ANDROID && !UNITY_EDITOR
            if(Application.version == data.live_android || Application.version == data.development_android)
            {
                forceUpdateScreen.SetActive(false);
            }
            else
            {
                forceUpdateScreen.SetActive(true);
            }
#elif UNITY_IOS
             if(Application.version == data.live_ios || Application.version == data.development_ios)
            {
                forceUpdateScreen.SetActive(false);
            }
            else
            {
                forceUpdateScreen.SetActive(true);
            }
#else
            if (Application.version == data.live_ios || Application.version == data.development_ios)
            {
                forceUpdateScreen.SetActive(false);
            }
            else
            {
                forceUpdateScreen.SetActive(true);
            }
#endif
        }
    }

    public void CheckAppVersion()
    {

        CheckAppVersionParams version = new CheckAppVersionParams
        {
#if UNITY_ANDROID && !UNITY_EDITOR
            platform = "android"
#elif UNITY_IOS
            platform = "ios"
#else
            platform = "ios"
#endif
        };

        StartCoroutine(RestfulAPI.Instance.UserManagementCallAPI<CheckAppVersionParams, CheckAppVersionData>(AppConstants.BASE_URL + AppConstants.APP_VERSION_URL, version, CheckAppVersionCallBack, UIManager.Instance.LoadingIndicator));
    }

    

    public void OpenLink()
    {
        string url = "";
#if UNITY_ANDROID
        url = androidURL;
#elif UNITY_IOS
        url = iosURL;
#endif

        Application.OpenURL(url);
    }


    public bool CheckAppVerResponseCode(int responseCode, string title, string message)
    {
        switch (responseCode)
        {
            case 200:
                return true;
            case -1:
                UIManager.Instance.DialogBox.CreateNewDialog(DialogType.ERROR, "WE CAN'T DETECT YOUR INTERNET CONNECTION", "Please check your internet connecton strength and try again.", "OK");
                UIManager.Instance.DialogBox.ShowDialog();
                return false;
            case 500:
                UIManager.Instance.DialogBox.CreateNewDialog(DialogType.ERROR, title, message, "OK");
                UIManager.Instance.DialogBox.ShowDialog();
                return false;
            case 422:
                UIManager.Instance.DialogBox.CreateNewDialog(DialogType.ERROR, title, message, "OK");
                UIManager.Instance.DialogBox.ShowDialog();
                return false;
            case 402:
                UIManager.Instance.DialogBox.CreateNewDialog(DialogType.ERROR, title, message, "OK");
                UIManager.Instance.DialogBox.ShowDialog();
                return false;
            case 401:
                UIManager.Instance.DialogBox.CreateNewDialog(
                    DialogType.ERROR,
                    "Session Expired",
                    "Please login again to continue.",
                    "OK",
                    delegate {
                        UserDataSessionManager.Instance.ClearUserData();
                        UIManager.Instance.Marker.SetActive(false);
                        UIManager.Instance.BackgroundCanvas.SetActive(true);
                        UIManager.Instance.InitialCamera.SetActive(true);
                        UIManager.Instance.ARCamera.SetActive(false);
                        UIManager.Instance.InitialCanvas.SetActive(true);
                        UIManager.Instance.Splash2.SetActive(true);
                        UIManager.Instance.ARCanvas.SetActive(false);

                        UIManager.Instance.behaviour.enabled = false;
                        UIManager.Instance.behaviour2.enabled = false;
                        UIManager.Instance.behaviour3.enabled = false;
                        UIManager.Instance.behaviour4.enabled = false;
                        UIManager.Instance.behaviour5.enabled = false;
                        UIManager.Instance.santaBehaviour.enabled = false;
                        UIManager.Instance.santaBehaviour2.enabled = false;
                        UIManager.Instance.santaBehaviour3.enabled = false;
                        UIManager.Instance.santaBehaviour4.enabled = false;
                        UIManager.Instance.santaBehaviour5.enabled = false;

                        FirebaseChecker.Instance.LogFirebaseEvent("Login");

                    });
                UIManager.Instance.DialogBox.ShowDialog();
                return false;
            default:
                UIManager.Instance.DialogBox.CreateNewDialog(DialogType.ERROR, "ERROR", "Something went wrong. Please check and try again.", "OK");
                UIManager.Instance.DialogBox.ShowDialog();
                return false;
        }
    }
}
