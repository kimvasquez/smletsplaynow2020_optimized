﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using TMPro;
using UnityEngine.UI;
public class LoginController : BaseController
{
    [Header("LOGIN FIELD")]
    [Space(10)]
    public TMP_InputField txtEmail;
    public TMP_InputField txtPassword;

    public TextMeshProUGUI txtEmailAdd;
    public TextMeshProUGUI txtPass;

    public GameObject LoginPanel;
    public GameObject Splash2Panel;
    public AnimatedUIEntry LoginAnimation;

    public ProfileControler profile;
    public AnimatedUIEntry verifyPanel;
    public GameObject errorMessage;
    public TextMeshProUGUI txtverifyHeading;
    public Image verifyIconContainter;
    public Sprite verifySprite;
    private void LoginCallback(UserData userData)
    {
        if (CheckResponseCodeLogin(userData.ResponseCode, userData.title , userData.message, userData.errors))
        {
            UserDataSessionManager.Instance.SaveUserData(userData.first_name, userData.last_name, userData.email, userData.accessToken, userData.image_url);

            profile.downloadedProfilePic = null;
            profile.UpdateProfile();

            LoginAnimation.CallExitAnimation(-1);
            LoginAnimation.transform.parent.gameObject.SetActive(false);

            if (PlayerPrefs.GetInt("OnBoardingDone") != 1)
            {

                UIManager.Instance.OneTimeScreen.SetActive(true);
                FirebaseChecker.Instance.LogFirebaseEvent("Santa_On_Boarding_1");
                
                
            }
            else
            {
                FirebaseChecker.Instance.LogFirebaseEvent("Santa_AR_View");
                UIManager.Instance.OneTimeScreen.SetActive(false);
                UIManager.Instance.InitialCanvas.SetActive(false);
                UIManager.Instance.BackgroundCanvas.SetActive(false);
                UIManager.Instance.ARCanvas.SetActive(true);

                UIManager.Instance.ARCamera.SetActive(true);
                UIManager.Instance.Marker.SetActive(true);
                UIManager.Instance.InitialCamera.SetActive(false);
                UIManager.Instance.tabGroup.OnTabSelected(UIManager.Instance.initialTabButton);

                UIManager.Instance.santaBehaviour.enabled = true;
                UIManager.Instance.santaBehaviour2.enabled = true;
                UIManager.Instance.santaBehaviour3.enabled = true;
                UIManager.Instance.santaBehaviour4.enabled = true;
                UIManager.Instance.santaBehaviour5.enabled = true;
                UIManager.Instance.behaviour.enabled = false;
                UIManager.Instance.behaviour2.enabled = false;
                UIManager.Instance.behaviour3.enabled = false;
                UIManager.Instance.behaviour4.enabled = false;
                UIManager.Instance.behaviour5.enabled = false;
                UIManager.Instance.santaPose1.SetActive(false);
                UIManager.Instance.santaPose2.SetActive(false);
                UIManager.Instance.santaPose3.SetActive(false);
                UIManager.Instance.santaPose4.SetActive(false);
                UIManager.Instance.santaPose5.SetActive(false);
            }

            ResetInput();

            errorMessage.SetActive(false);
            //LoginPanel.SetActive(true);
            //Splash2Panel.SetActive(true);
            UIManager.Instance.Splash1.SetActive(false);
        }
    }

    public void Login()
    {
        if (txtPassword.text.Length <= 1 || txtEmail.text.Length <= 1)
        {
            //UIManager.Instance.DialogBox.ShowDialog();
            //UIManager.Instance.DialogBox.CreateNewDialog(DialogType.ERROR, "ERROR!", "Please fill in all the required field correctly.", "OK");
            UIManager.Instance.DialogText.CreateErrorLoginDialog("Please fill in all the required field correctly.");
            txtEmail.image.color = Color.red;
            txtPassword.image.color = Color.red;
            txtEmailAdd.color = Color.red;
            txtPass.color = Color.red;

            //loginButtonRect.anchoredPosition = new Vector2(0,-736.4f);
            //forgotPassRect.anchoredPosition = new Vector2(0, -869f);

            errorMessage.SetActive(true);
        }
        else
        {
            LoginParams loginParams = new LoginParams
            {
                email = txtEmail.text.TrimEnd('\u200b'),
                password = txtPassword.text.TrimEnd('\u200b'),
                device_id = DeviceType()
            };
         
            StartCoroutine(RestfulAPI.Instance.UserManagementCallAPI<LoginParams, UserData>(AppConstants.BASE_URL + AppConstants.LOGIN_URL, loginParams, LoginCallback, UIManager.Instance.LoadingIndicator));

        }
    }

    public void ResetInput()
    {
        txtEmail.text = txtPassword.text = "";
        UIManager.Instance.DialogText.txtErrorLoginDialog.text = "";
        txtEmail.image.color = new Color32(231,231,231,255);
        txtPassword.image.color = new Color32(231, 231, 231, 255);
        txtEmailAdd.color = new Color32(231, 231, 231, 255);
        txtPass.color = new Color32(231, 231, 231, 255);
    }

    public void ShowPrivacyPass()
    {
        if(txtPassword.contentType == TMP_InputField.ContentType.Password)
        {
            txtPassword.contentType = TMP_InputField.ContentType.Standard;
            //txtPassword.ForceLabelUpdate();
        }
        else
        {
            txtPassword.contentType = TMP_InputField.ContentType.Password;
            //txtPassword.ForceLabelUpdate();
        }
    }

    public bool CheckResponseCodeLogin(int responseCode, string title, string message, string errors)
    {
        {
            switch (responseCode)
            {
                case 200:
                    errorMessage.SetActive(false);
                    return true;
                case -1:
                    //UIManager.Instance.DialogBox.CreateNewDialog(DialogType.ERROR, "WE CAN'T DETECT YOUR INTERNET CONNECTION", "Please check your internet connecton strength and try again.", "OK");
                    //UIManager.Instance.DialogBox.ShowDialog();
                    UIManager.Instance.DialogText.CreateErrorLoginDialog("Please check your internet connecton and try again.");
                    txtEmail.image.color = Color.red;
                    txtPassword.image.color = Color.red;
                    txtEmailAdd.color = Color.red;
                    txtPass.color = Color.red;
                    //loginButtonRect.anchoredPosition = new Vector2(0, -736.4f);
                    //forgotPassRect.anchoredPosition = new Vector2(0, -869f);

                    errorMessage.SetActive(true);
                    return false;
                case 500:
                    //UIManager.Instance.DialogBox.CreateNewDialog(DialogType.ERROR, title, message, "OK");
                    //UIManager.Instance.DialogBox.ShowDialog();
                    UIManager.Instance.DialogText.CreateErrorLoginDialog(message);
                    txtEmail.image.color = Color.red;
                    txtPassword.image.color = Color.red;
                    txtEmailAdd.color = Color.red;
                    txtPass.color = Color.red;
                    //loginButtonRect.anchoredPosition = new Vector2(0, -736.4f);
                    //forgotPassRect.anchoredPosition = new Vector2(0, -869f);

                    errorMessage.SetActive(true);
                    return false;
                case 422:
                    //UIManager.Instance.DialogBox.CreateNewDialog(DialogType.ERROR, title, message, "OK");
                    //UIManager.Instance.DialogBox.ShowDialog();
                    UIManager.Instance.DialogText.CreateErrorLoginDialog(message);
                    Debug.Log("Response Message: " + message);
                    //foreach(var messages in errors)
                    //{
                    //    Debug.Log("Response Message: " + messages);
                    //}
                    txtEmail.image.color = Color.red;
                    txtPassword.image.color = Color.red;
                    txtEmailAdd.color = Color.red;
                    txtPass.color = Color.red;
                    //loginButtonRect.anchoredPosition = new Vector2(0, -736.4f);
                    //forgotPassRect.anchoredPosition = new Vector2(0, -869f);
                    if(title == "ACCOUNT IS NOT YET VERIFIED")
                    {
                        verifyPanel.transform.parent.gameObject.SetActive(true);
                        verifyPanel.CallEntryAnimation(-1);
                        txtverifyHeading.text = "Verify your account";
                        verifyIconContainter.sprite = verifySprite;
                        ResetInput();
                        errorMessage.SetActive(false);

                    }
                    errorMessage.SetActive(true);
                    return false;
                case 402:
                    //UIManager.Instance.DialogBox.CreateNewDialog(DialogType.ERROR, title, message, "OK");
                    //UIManager.Instance.DialogBox.ShowDialog();
                    UIManager.Instance.DialogText.CreateErrorLoginDialog(message);
                    txtEmail.image.color = Color.red;
                    txtPassword.image.color = Color.red;
                    txtEmailAdd.color = Color.red;
                    txtPass.color = Color.red;
                    //loginButtonRect.anchoredPosition = new Vector2(0, -736.4f);
                    //forgotPassRect.anchoredPosition = new Vector2(0, -869f);

                    errorMessage.SetActive(true);
                    return false;
                default:
                    //UIManager.Instance.DialogBox.CreateNewDialog(DialogType.ERROR, "ERROR", "Something went wrong. Please check and try again.", "OK");
                    //UIManager.Instance.DialogBox.ShowDialog();
                    UIManager.Instance.DialogText.CreateErrorLoginDialog("Something went wrong. Please check and try again.");
                    txtEmail.image.color = Color.red;
                    txtPassword.image.color = Color.red;
                    txtEmailAdd.color = Color.red;
                    txtPass.color = Color.red;
                    //loginButtonRect.anchoredPosition = new Vector2(0, -736.4f);
                    //forgotPassRect.anchoredPosition = new Vector2(0, -869f);

                    errorMessage.SetActive(true);
                    return false;
            }
        }
    }

}
