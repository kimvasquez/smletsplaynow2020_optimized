﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using TMPro;


public class ForgotPasswordController : BaseController
{
    [Header("FORGOT FIELD")]
    [Space(10)]
    public TMP_InputField txt_forgot_email;

    public AnimatedUIEntry loginPanel;
    public AnimatedUIEntry forgotPassPanel;

    public void ResetPasswordCallback(ForgotPassData forgotPassData)
    {
        if (CheckResponseCode(forgotPassData.ResponseCode, forgotPassData.title, forgotPassData.message))
        {
            UIManager.Instance.DialogBox.CreateNewDialog(DialogType.SUCCESS, forgotPassData.title, forgotPassData.message, "OK", delegate {
                txt_forgot_email.text = "";
                forgotPassPanel.CallExitAnimation(-1);
                loginPanel.transform.parent.gameObject.SetActive(true);
                loginPanel.CallEntryAnimation(-1);
                FirebaseChecker.Instance.LogFirebaseEvent("Login");
            });
            UIManager.Instance.DialogBox.ShowDialog();
        }
        else
        {
            txt_forgot_email.text = "";
        }
    }

    public void ForgotPassword()
    {
        if (txt_forgot_email.text.Length > 1)
        {

            ForgotPassParams forgotPassParams = new ForgotPassParams
            {
                email = txt_forgot_email.text.TrimEnd('\u200b'),
            };

            StartCoroutine(RestfulAPI.Instance.UserManagementCallAPI<ForgotPassParams, ForgotPassData>(AppConstants.BASE_URL + AppConstants.RESET_PASS_URL, forgotPassParams, ResetPasswordCallback, UIManager.Instance.LoadingIndicator));

        }
        else
        {

            UIManager.Instance.DialogBox.CreateNewDialog(DialogType.ERROR, "ERROR!", "Please fill in all the required field correctly.", "OK");
            UIManager.Instance.DialogBox.ShowDialog();
        }
    }
}
