﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BaseController : MonoBehaviour
{
    public bool CheckResponseCode(int responseCode, string title, string message)
    {
        switch (responseCode)
        {
            case 200:
                return true;
            case -1:
                UIManager.Instance.DialogBox.CreateNewDialog(DialogType.ERROR, "WE CAN'T DETECT YOUR INTERNET CONNECTION", "The App requires a steady Internet connection. Please check your settings and try again.", "OK");
                UIManager.Instance.DialogBox.ShowDialog();
                return false;
            case 500:
                UIManager.Instance.DialogBox.CreateNewDialog(DialogType.ERROR, title, message, "OK");
                UIManager.Instance.DialogBox.ShowDialog();
                return false;
            case 422:
                UIManager.Instance.DialogBox.CreateNewDialog(DialogType.ERROR, title, message, "OK");
                UIManager.Instance.DialogBox.ShowDialog();
                return false;
            case 402:
                UIManager.Instance.DialogBox.CreateNewDialog(DialogType.ERROR, title, message, "OK");
                UIManager.Instance.DialogBox.ShowDialog();
                return false;
            case 401:
                UIManager.Instance.DialogBox.CreateNewDialog(
                    DialogType.ERROR, 
                    "Session Expired", 
                    "Please login again to continue.", 
                    "OK", 
                    delegate {
                        UserDataSessionManager.Instance.ClearUserData();
                        UIManager.Instance.Marker.SetActive(false);
                        UIManager.Instance.BackgroundCanvas.SetActive(true);
                        UIManager.Instance.InitialCamera.SetActive(true);
                        UIManager.Instance.ARCamera.SetActive(false);
                        UIManager.Instance.InitialCanvas.SetActive(true);
                        UIManager.Instance.Splash2.SetActive(true);
                        UIManager.Instance.ARCanvas.SetActive(false);

                        UIManager.Instance.behaviour.enabled = false;
                        UIManager.Instance.behaviour2.enabled = false;
                        UIManager.Instance.behaviour3.enabled = false;
                        UIManager.Instance.behaviour4.enabled = false;
                        UIManager.Instance.behaviour5.enabled = false;
                        UIManager.Instance.santaBehaviour.enabled = false;
                        UIManager.Instance.santaBehaviour2.enabled = false;
                        UIManager.Instance.santaBehaviour3.enabled = false;
                        UIManager.Instance.santaBehaviour4.enabled = false;
                        UIManager.Instance.santaBehaviour5.enabled = false;

                        FirebaseChecker.Instance.LogFirebaseEvent("Login");

                    });
                UIManager.Instance.DialogBox.ShowDialog();
                return false;
            default:
                UIManager.Instance.DialogBox.CreateNewDialog(DialogType.ERROR, "ERROR", "Please check and try again.", "OK");
                UIManager.Instance.DialogBox.ShowDialog();
                return false;
        }
    }

    public string DeviceType()
    {

#if UNITY_IOS
            return "ios";
#elif UNITY_ANDROID
            return "android";
#else
        return "android";
#endif
    }
}
