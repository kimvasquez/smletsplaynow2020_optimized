﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using TMPro;
public class VerifyAccountController : BaseController
{
    [Header("LOGIN FIELD")]
    [Space(10)]
    public string txtEmail;

    public AnimatedUIEntry verifyPanel;
    public AnimatedUIEntry loginPanel;

    private void Awake()
    {
        txtEmail = PlayerPrefs.GetString("RegEmail"); 
    }
    private void VerifyCallback(VerifyData verifyData)
    {
        if (CheckResponseCode(verifyData.ResponseCode, verifyData.title, verifyData.message))
        {

            UIManager.Instance.DialogBox.CreateNewDialog(DialogType.SUCCESS,
                "RESEND SUCCESSFULL",
                "Thank you! A verification link has been resent to your e-mail. Click on the link to verify your account.",
                "OK",
                delegate {
                    verifyPanel.CallExitAnimation(1);
                    loginPanel.transform.parent.gameObject.SetActive(true);
                    loginPanel.CallEntryAnimation(-1);
                    FirebaseChecker.Instance.LogFirebaseEvent("Login");
                });
            UIManager.Instance.DialogBox.ShowDialog();

        }
    }

    public void Verify()
    {
        txtEmail = PlayerPrefs.GetString("RegEmail");

        VerifyParams verifyParams = new VerifyParams
        {
            email = txtEmail.TrimEnd('\u200b'),
        };

        StartCoroutine(RestfulAPI.Instance.UserManagementCallAPI<VerifyParams, VerifyData>(AppConstants.BASE_URL + AppConstants.VERIFICATION_URL, verifyParams, VerifyCallback, UIManager.Instance.LoadingIndicator));
    }
}
