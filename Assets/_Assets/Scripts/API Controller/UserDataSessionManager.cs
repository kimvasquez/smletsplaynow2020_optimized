﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class UserDataSessionManager : MonoBehaviour
{

    private static UserDataSessionManager _instance;

    public static UserDataSessionManager Instance
    {
        get
        {
            if (_instance == null)
            {
                _instance = FindObjectOfType<UserDataSessionManager>();
                if (_instance == null)
                {
                    GameObject gameObject = new GameObject();
                    gameObject.name = typeof(UserDataSessionManager).Name;
                    _instance = gameObject.AddComponent<UserDataSessionManager>();
                    DontDestroyOnLoad(gameObject);
                }
            }
            return _instance;
        }
    }

    public string AccessToken { private set
        {
            PlayerPrefs.SetString("AccessToken", value);
        }
        get
        {
            return PlayerPrefs.GetString("AccessToken");
        }
    }
    public string UserFirstName
    {
        private set
        {
            PlayerPrefs.SetString("UserFName", value);
        }
        get
        {
            return PlayerPrefs.GetString("UserFName");
        }
    }
    public string UserLastName
    {
        private set
        {
            PlayerPrefs.SetString("UserLName", value);
        }
        get
        {
            return PlayerPrefs.GetString("UserLName");
        }
    }
    public string UserEmail
    {
        private set
        {
            PlayerPrefs.SetString("UserEmail", value);
        }
        get
        {
            return PlayerPrefs.GetString("UserEmail");
        }
    }
    public string ImageURL
    {
        set
        {
            PlayerPrefs.SetString("ImageURL", value);
        }
        get
        {
            return PlayerPrefs.GetString("ImageURL");
        }
    }

    public void SaveUserData(string fName, string lName, string email, string accessToken, string imageURL)
    {
        UserFirstName = fName;
        UserLastName = lName;
        UserEmail = email;
        AccessToken = accessToken;
        ImageURL = imageURL;
    }

    public void ClearUserData()
    {
        AccessToken = UserFirstName = UserLastName = UserEmail = ImageURL =  "";
        PlayerPrefs.DeleteKey("AccessToken");
        PlayerPrefs.DeleteKey("UserFName");
        PlayerPrefs.DeleteKey("UserLName");
        PlayerPrefs.DeleteKey("UserEmail");
        PlayerPrefs.DeleteKey("ImageURL");
    }
}
