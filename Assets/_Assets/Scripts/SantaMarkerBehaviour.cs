﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Vuforia;
public class SantaMarkerBehaviour : DefaultTrackableEventHandler
{
    private TrackableBehaviour m_trackableBehaviour;
    public GameObject objectToShow;

    private void Awake()
    {
        m_trackableBehaviour = GetComponent<TrackableBehaviour>();
        if (m_trackableBehaviour)
        {
            m_trackableBehaviour.RegisterOnTrackableStatusChanged(OnTrackableStateChanged);
        }

    }

    public void OnTrackableStateChanged(TrackableBehaviour.StatusChangeResult obj)
    {
        if (obj.NewStatus == TrackableBehaviour.Status.DETECTED || obj.NewStatus == TrackableBehaviour.Status.TRACKED || obj.NewStatus == TrackableBehaviour.Status.EXTENDED_TRACKED)
        {
            OnTrackingFound();
        }
        else
        {
            OnTrackingLost();

        }
    }
}
