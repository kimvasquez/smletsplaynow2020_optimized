﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;


public class LocationService : MonoBehaviour
{

    public static LocationService Instance { get; protected set; }
    [HideInInspector]
    public double longitude;
    [HideInInspector]
    public double latitude;

    public bool isLocationServiceAvailable;

    private void Awake()
    {
        Instance = this;
        DontDestroyOnLoad(gameObject);
    }

    private void Start()
    {
        StartCoroutine(StartLocationService());
        InvokeRepeating("TryOpeningLocation", .5f, .5f);
    }

    public void RestartLocationService()
    {
        StartCoroutine(StartLocationService());
    }

    IEnumerator StartLocationService()
    {

#if UNITY_IOS && !UNITY_EDITOR
        Input.location.Start();
#endif

        if (!Input.location.isEnabledByUser)
        {
#if UNITY_ANDROID && !UNITY_EDITOR
            UIManager.Instance.CheckLocationPermission();
#endif
            isLocationServiceAvailable = false;   
            yield break;
        }

        Input.location.Start();

        int maxWait = 20;

        while (Input.location.status == LocationServiceStatus.Initializing && maxWait > 0)
        {
            yield return new WaitForSeconds(1);
            maxWait--;
        }

        if (maxWait <= 0)
        {
            Debug.Log("Connection Timeout");

            isLocationServiceAvailable = false;
            yield break;
        }

        if (Input.location.status == LocationServiceStatus.Failed)
        {
            Debug.Log("Connection Failed");
       
            isLocationServiceAvailable = false;
            yield break;
        }

        isLocationServiceAvailable = true;
        longitude = Input.location.lastData.longitude;
        latitude = Input.location.lastData.latitude;

        yield break;
    }

    private void Update()
    {
        if (isLocationServiceAvailable)
        {
            longitude = Input.location.lastData.longitude;
            latitude = Input.location.lastData.latitude;
        }
    }

    private void TryOpeningLocation()
    {
        if (!Input.location.isEnabledByUser && !UIManager.Instance.DialogBox.dialogWindow.activeSelf && UIManager.Instance.ARCanvas.activeSelf)
        {
            RestartLocationService();
        }
    }
}
