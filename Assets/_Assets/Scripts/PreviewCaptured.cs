﻿using NatSuite.Sharing;
using System.Collections;
using System.Collections.Generic;
using System.IO;
using UnityEngine;

public class PreviewCaptured : MonoBehaviour
{
    public RectTransform rectT;

    int width;
    int height;
    private void Start()
    {
        width = System.Convert.ToInt32(rectT.rect.width);
        height = System.Convert.ToInt32(rectT.rect.height);

        StartCoroutine(SavePhoto());
    }

    IEnumerator SavePhoto()
    {
        imageToShare = null;
        yield return new WaitForSeconds(2f);
        yield return new WaitForEndOfFrame();
        Vector3[] corners = new Vector3[4];
        rectT.GetWorldCorners(corners);
        var startX = corners[0].x;
        var startY = corners[0].y;

        width = (int)corners[3].x - (int)corners[0].x;
        height = (int)corners[2].y - (int)corners[0].y;

        imageToShare = new Texture2D(width, height, TextureFormat.RGB24, false);
        imageToShare.ReadPixels(new Rect(startX, startY, width, height), 0, 0);
        imageToShare.Apply();

        byte[] imageBytes = imageToShare.EncodeToPNG();
        string filePath = Path.Combine(Application.temporaryCachePath, "Shared_Img.png");
        File.WriteAllBytes(filePath, imageBytes);


        NativeGallery.SaveImageToGallery(imageBytes, "#LetsPlayNowAtSM", "#LetsPlayNowAtSM" + System.DateTime.Now.ToString("yyyyMMddhhmmss") + ".png", null);
    }

    public void SharePhoto()
    {
#if UNITY_ANDROID
        if (!UIManager.Instance.CheckGalleryWritePermission())
        {
            return;
        }
#endif
        StartCoroutine(ShareScreenshot());
    }

    Texture2D imageToShare;
    IEnumerator ShareScreenshot()
    {
        imageToShare = null;
        yield return new WaitForEndOfFrame();

        Vector3[] corners = new Vector3[4];
        rectT.GetWorldCorners(corners);
        var startX = corners[0].x;
        var startY = corners[0].y;

        width = (int)corners[3].x - (int)corners[0].x;
        height = (int)corners[2].y - (int)corners[0].y;

        imageToShare = new Texture2D(width, height, TextureFormat.RGB24, false);
        imageToShare.ReadPixels(new Rect(startX, startY, width, height), 0, 0);
        imageToShare.Apply();

        //SharePhotoFunction();
        
        ShareFunc();



    }

    //async void SharePhotoFunction()
    //{
    //    var isSuccess = await new SharePayload().AddImage(imageToShare).Commit();
    //    if(isSuccess)
    //    {
    //        FirebaseChecker.Instance.LogFirebaseEvent("Photo_Shared");
    //    }
    //}

    private void ShareFunc()
    {

        byte[] imageBytes = imageToShare.EncodeToPNG();
        string filePath = Path.Combine(Application.temporaryCachePath, "Shared_Img.png");
        File.WriteAllBytes(filePath, imageBytes);


        Destroy(imageToShare);

        new NativeShare().AddFile(filePath)
            .SetCallback((result, shareTarget) => ResultShareCallBack(result,shareTarget))
            .Share();
    }

    private void ResultShareCallBack(NativeShare.ShareResult result, string shareTarget)
    {
        if(result == NativeShare.ShareResult.Shared)
        {
            FirebaseChecker.Instance.LogFirebaseEvent("Photo_Shared");
        }
        else if (result == NativeShare.ShareResult.Unknown)
        {
            FirebaseChecker.Instance.LogFirebaseEvent("Photo_Shared");
        }
        else if (result == NativeShare.ShareResult.NotShared)
        {
            FirebaseChecker.Instance.LogFirebaseEvent("Photo_Shared");
        }

    }
}
