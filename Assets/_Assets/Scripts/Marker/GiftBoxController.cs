﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Playables;
using UnityEngine.EventSystems;

public class GiftBoxController : MonoBehaviour
{
    public PlayableDirector director;
    public bool isPaused = false;
    public ScanController scanController;
    private BoxCollider boxCollider;
    public GameObject TapMeIndicators;



    private void Awake()
    {
        director.paused += OnPause;
        boxCollider = GetComponent<BoxCollider>();
    }

    private void OnEnable()
    {
        boxCollider.enabled = isPaused;
        if (!isPaused)
        {
            director.time = 0;
            director.Play();
        }
        else
        {
            director.time = .65f;
            director.Pause();
            isSending = false;
        }    
    }

    private void OnDisable()
    {
        if (!isPaused)
        {
            director.time = 0;
            director.Play();
            isPaused = false;
            isSending = false;
        }
        StopAllCoroutines();
    }

    public void OnPause(PlayableDirector director)
    {
        TapMeIndicators.SetActive(true);
        boxCollider.enabled = true;
    }

    private void Update()
    {
        if (!isPaused && !isSending)
        {
            if (director.time >= .65)
            {
                director.Pause();
                isPaused = true;
            }
        }
    }

    private void OnMouseDown()
    {
        if (EventSystem.current.IsPointerOverGameObject(-1))
        {
            return;
        }
        if (!isSending)
        {
            TapMeIndicators.SetActive(false);
            StartCoroutine(SendScan());
        }
    }

    public bool isSending = false;

    private IEnumerator SendScan()
    {
        isPaused = false;
        isSending = true;
        director.Play();
        yield return new WaitForSecondsRealtime(3f);
        scanController.OnBoxTap(delegate
        {
            isPaused = isSending = false;
            TapMeIndicators.SetActive(false);
            UIManager.Instance.Marker.SetActive(false);
        });
    }
}
