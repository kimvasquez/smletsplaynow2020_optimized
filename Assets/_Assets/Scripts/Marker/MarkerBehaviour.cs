﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Vuforia;

public class MarkerBehaviour : DefaultTrackableEventHandler
{
    public static bool isTracked;

    private TrackableBehaviour m_trackableBehaviour;
    public AudioSource musicPlayer;
    public GameObject objectToShow;
    private void OnDisable()
    {
        if (musicPlayer != null)
        {
            musicPlayer.Pause();
        }
    }

    private void Awake()
    {
        m_trackableBehaviour = GetComponent<TrackableBehaviour>();
        if (m_trackableBehaviour)
        {
            m_trackableBehaviour.RegisterOnTrackableStatusChanged(OnTrackableStateChanged);
        }

    }

    public void OnTrackableStateChanged(TrackableBehaviour.StatusChangeResult obj)
    {
        if (obj.NewStatus == TrackableBehaviour.Status.DETECTED || obj.NewStatus == TrackableBehaviour.Status.TRACKED || obj.NewStatus == TrackableBehaviour.Status.EXTENDED_TRACKED)
        {
#if !UNITY_EDITOR
            if (!LocationService.Instance.isLocationServiceAvailable)
            {
                LocationService.Instance.RestartLocationService();
                UIManager.Instance.DialogBox.CreateNewDialog(DialogType.ERROR, "UNABLE TO GET LOCATION", "Please make sure that your location is turned on in order to use the app.", "OK");
                UIManager.Instance.DialogBox.ShowDialog();
                objectToShow.SetActive(false);
                return;
            }
#endif
            isTracked = true;

            objectToShow.SetActive(true);
            musicPlayer.Play();
            //Debug.Log("Trackable Name: " + imageTarget.ImageTarget);
            if (m_trackableBehaviour.TrackableName == "AweSMMarker1")
            {
                UIManager.Instance.markerID = 1;
            }
            else if (m_trackableBehaviour.TrackableName.ToString() == "AweSMMarker2")
            {
                //Debug.Log("ImageTarget2");
                UIManager.Instance.markerID = 2;
            }
            else if (m_trackableBehaviour.TrackableName.ToString() == "AweSMMarker3")
            {
                //Debug.Log("ImageTarget3");
                UIManager.Instance.markerID = 3;
            }
            else if (m_trackableBehaviour.TrackableName.ToString() == "AweSMMarker4")
            {
                //Debug.Log("ImageTarget4");
                UIManager.Instance.markerID = 4;
            }
            else if (m_trackableBehaviour.TrackableName.ToString() == "AweSMMarker5")
            {
                //Debug.Log("ImageTarget5");
                UIManager.Instance.markerID = 5;
            }

            OnTrackingFound();
        }
        else
        {
            isTracked = false;
            objectToShow.SetActive(false);
            musicPlayer.Pause();
            OnTrackingLost();
        }


    }



}